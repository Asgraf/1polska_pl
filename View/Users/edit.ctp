<?php
/**
 * @var $this View
 * @var $title_for_layout
 */
$this->Html->scriptBlock(
<<<SCRIPT

$(function(){
	$('.avatar_select input[disabled]').parents('label').on('click',function(){
		alert($(this).find('div[title]').attr('title'));
	}).addClass('disabled2')
});

SCRIPT
	,['block'=>'script']);
$avatar_options = [
	null=>'Gravatar',
	'Google'=>'Google',
	'Facebook'=>'Facebook',
];
$disabled_avatar_options = [];
foreach($avatar_options as $key=>&$val) {
	if($key && !$this->Form->value('User.metadata.'.$key)) {
		$disabled_avatar_options[]=$key;
		$val = '<div title="'.__('Wybranie tej opcji wymaga wcześniejszego zalogowania się za pomocą %s\'a',$key).'">'.$this->Html->image(
				'anonim.jpg',
				['width'=>120,'height'=>120]
			).'<p>'.$this->Html->image(($key).'.png').' '.$val.'</p></div>';
	} else {
		$val = '<div>'.$this->Html->image(
			getAvatarUrl(AuthComponent::user(),200,$key?:'Gravatar'),
			['width'=>120,'height'=>120]
		).'<p>'.$this->Html->image(($key?:'Gravatar').'.png').' '.$val.'</p></div>';
	}
}
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=ucfirst($title_for_layout);?></legend>
	<?php
	echo $this->Form->input('username', ['label'=>__d('user','Nazwa Użytkownika')]);
	echo $this->Form->input('password', ['required'=>false,'value'=>false,'label'=>__d('user','New Password')]);
	?>
		<div class="form-group">
			<label class="col col-md-3 control-label" for="UserPassword">Avatar</label>
			<div class="col col-md-9">
			<?=$this->Form->input('avatar_from', ['div'=>'avatar_select btn-group','wrapInput'=>false,'legend'=>false,'class'=>'btn btn-default text-center','type'=>'radio','options'=>$avatar_options,'disabled'=>$disabled_avatar_options,'escape'=>false]);?>
			</div>
		</div>
	<?php
	echo $this->element('recaptcha');
	?>
	</fieldset>
	<?php
	echo $this->Form->button(__d('user','Submit'),array('class'=>'btn submit','data-icon'=>'ok'));
	echo $this->Html->link('Usuń moje konto',['action'=>'delete'],['class'=>'pull-right text-danger']);
	echo $this->Form->end();
	?>
</div>