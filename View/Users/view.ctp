<?php
/**
 * @var $this View
 * @var $user
 */
echo $this->element('Asgraf.related_buttons');
if(AuthComponent::user('admin')) {
	echo $this->Html->link('Pokaż w panelu admina',array('admin'=>true,'action'=>'view','emailhash'=>$this->request['emailhash'],'slug'=>$this->request['slug']),array('class'=>'btn btn-info'));
}
?>
<div class="users view">
<h2><?php  echo h(ucfirst($user['User']['username'])) ?></h2>
	<?php echo $this->Html->image(
		getAvatarUrl($user['User'],200),
		['width'=>200,'height'=>200]
	);
	?>
	<dl>
		<dt>Data rejestracji</dt>
		<dd class="created"><?php echo $this->Format->datetime($user['User']['created']); ?></dd>
		<dt>Domena konta email</dt>
		<dd class="emaildomain"><?php echo h(ucfirst($user['User']['emaildomain'])); ?></dd>
	</dl>
	<section class="container">
		<div class="row">
			<div class="col-md-6">
				<h3>Reprezentanci, którym ufam:</h3>
				<ul>
					<?php
					$count = 0;
					foreach($user['Representative'] as $representative) {
						if($representative['RepresentativesUser']['value']==1) {
							$url = ['controller'=>'representatives','action'=>'view','id'=>$representative['id'],'slug'=>slug($representative['name'])];
							echo '<li class=""><a href="'.$this->Html->url($url).'">';
							if(!empty($representative['Photo']['id'])) {
								echo $this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$representative['Photo']['id'],'access_key'=>$representative['Photo']['access_key'],16,16,1),array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
							} else {
								echo $this->Html->image('anonim.jpg',array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
							}
							echo '&nbsp;';
							echo $representative['name'];
							echo '</a></li>';
							$count++;
						}
					}
					if(!$count) {
						echo '<li>Brak</li>';
					}
					?>
				</ul>
			</div>
			<div class="col-md-6">
				<h3>Reprezentanci, którym nie ufam:</h3>
				<ul>
					<?php
					$count = 0;
					foreach($user['Representative'] as $representative) {
						if($representative['RepresentativesUser']['value']==-1) {
							$url = ['controller'=>'representatives','action'=>'view','id'=>$representative['id'],'slug'=>slug($representative['name'])];
							echo '<li class=""><a href="'.$this->Html->url($url).'">';
							if(!empty($representative['Photo']['id'])) {
								echo $this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$representative['Photo']['id'],'access_key'=>$representative['Photo']['access_key'],16,16,1),array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
							} else {
								echo $this->Html->image('anonim.jpg',array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
							}
							echo '&nbsp;';
							echo $representative['name'];
							echo '</a></li>';
							$count++;
						}
					}
					if(!$count) {
						echo '<li>Brak</li>';
					}
					?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3>Postulaty, które popieram:</h3>
				<ul style="-webkit-column-width: 300px;-moz-column-width: 300px;column-width: 300px;">
					<?php
					$count = 0;
					foreach($user['Postulate'] as $postulate) {
						if($postulate['PostulatesUser']['value']==1) {
							$url = ['controller'=>'postulates','action'=>'view','id'=>$postulate['id'],'slug'=>slug($postulate['name'])];
							echo '<li class=""><a href="'.$this->Html->url($url).'">';
							echo $postulate['name'];
							echo '</a></li>';
							$count++;
						}
					}
					if(!$count) {
						echo '<li>Brak</li>';
					}
					?>
				</ul>
			</div>
		</div>
	</section>
</div>