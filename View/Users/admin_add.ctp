<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="users form">
	<?php echo $this->Form->create('User'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__d('user','User');?></legend>
		<?php
		echo $this->Form->input('username',array('label'=>__d('user','Username')));
		echo $this->Form->input('password',array('label'=>__d('user','Password')));
		echo $this->Form->input('email',array('label'=>__d('user','Email')));
		echo $this->Form->input('status',array('options'=>$status,'label'=>__d('user','Status')));
		echo $this->element('recaptcha');
		?>
	</fieldset>
	<?php echo $this->Form->button(__d('user','Submit'),array('class'=>'submit','data-icon'=>'ok')); ?>
	<?php echo $this->Form->end(); ?>
</div>
<nav class="actions">
	<ul>
		<li><?php echo $this->Html->link(__d('user','List Users'),array('action'=>'index'),array('class'=>'index','data-role'=>'button','data-inline'=>'true','data-icon'=>'list-ol')); ?></li>
	</ul>
</nav>