<?php
/**
 * @var $this View
 * @var $title_for_layout
 */
echo $this->element('Asgraf.related_buttons'); ?>
<div class="users form">
	<?php echo $this->Form->create('User'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=ucfirst($title_for_layout);?></legend>
		<div class="form-group">
			<label class="col col-md-3 control-label">Czy na pewno chcesz usunąć swoje konto?</label>
			<div class="col col-md-9 required">
				<?php
				echo $this->Form->input('confirm1', ['required','type'=>'checkbox','div'=>false,'wrapInput'=>false,'class'=>false,'label'=>['text'=>'Tak, chcę usunąć swoje konto','class'=>false]]);
				echo $this->Form->input('confirm2', ['required','type'=>'checkbox','div'=>false,'wrapInput'=>false,'class'=>false,'label'=>['text'=>'Oraz, anulować wszelkie oddane przeze mnie głosy','class'=>false]]);
				echo $this->Form->input('confirm3', ['required','type'=>'checkbox','div'=>false,'wrapInput'=>false,'class'=>false,'label'=>['text'=>'Zdaję sobie sprawę że jest to operacja nie odwracalna','class'=>false]]);
				echo $this->Form->input('confirm4', ['required','type'=>'checkbox','div'=>false,'wrapInput'=>false,'class'=>false,'label'=>['text'=>'I nie przekonacie mnie, do zmiany zdania ;)','class'=>false]]);
				?>
			</div>
		</div>
		<?=$this->element('recaptcha');?>
	</fieldset>
	<?php
	echo $this->Form->button('Usuń moje konto',array('class'=>'btn btn-danger submit pull-right'));
	echo $this->Html->link('Nie chcę usuwać konta','/',['class'=>'btn btn-success']);
	echo $this->Form->end();
	?>
</div>