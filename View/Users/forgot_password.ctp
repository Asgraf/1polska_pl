<?php
/**
 * @var $this View
 * @var $title_for_layout
 */
$this->set('title_for_layout','Zapomniane hasło');
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=ucfirst($title_for_layout);?></legend>
	<?php
	echo $this->Form->input('email',array('label'=>__d('user','Email')));
	echo $this->element('recaptcha');
	?>
	</fieldset>
<?php echo $this->Form->button(__d('user','Submit'),array('class'=>'btn submit','data-icon'=>'ok')); ?>
<?php echo $this->Form->end(); ?>
</div>