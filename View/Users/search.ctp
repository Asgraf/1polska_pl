<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php unset($this->request['_Token']); ?>
<div class="users form">
<?php echo $this->Form->create('User',array('action'=>'index','type'=>'get')); ?>
<?php $this->Form->inputDefaults(array('required'=>'false')); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__d('user','User');?></legend>
	<?php
		echo $this->Form->input('username',array('label'=>__d('user','Username')));
		echo $this->Form->input('status',array('multiple'=>'multiple','options'=>$status,'label'=>__d('user','Status')));
	?>
	</fieldset>
<?php echo $this->Form->submit(__d('cake','Search'),array('class'=>'submit','data-role'=>'button','data-icon'=>'search')); ?>
<?php echo $this->Form->end(); ?>
</div>
<nav class="actions">
	<ul>
		<li><?php echo $this->Html->link(__d('user','List Users'),array('action'=>'index'),array('class'=>'index','data-role'=>'button','data-inline'=>'true','data-icon'=>'list-ol')); ?></li>
	</ul>
</nav>