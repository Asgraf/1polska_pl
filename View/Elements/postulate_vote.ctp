<?php
/**
 * @var $this View
 * @var $postulate
 */
$voteup_class = '';
$div_title = 'Ten postulat nie ma Twojego poparcia';
$value = AuthComponent::user('PostulatesUser.'.$postulate['Postulate']['id'].'.value');

$voteup_url =  array(
	'plugin'=>false,
	'admin'=>false,
	'controller'=>'postulates',
	'action'=>'cancelvote',
	'id'=>$postulate['Postulate']['id']
);
if($value==1) {
	$voteup_class = 'active';
	$voteup_title = $this->Html->image('kratka_ptaszek.png').'Popieram';
	$div_title = 'Popierasz ten postulat';
} else {
	$voteup_url['action']='upvote';
	$voteup_title = $this->Html->image('kratka_bez_ptaszka.png').'Popieram';
}
?>
<div id="vote_widget_postulate_<?=$postulate['Postulate']['id']?>" title="<?=$div_title?>" class="postulate_vote btn-group">
	<?php
	echo '<div class="btn btn-success default_cursor">'.(Hash::get($postulate,'Postulate.upvotes')).'</div>';
	echo $this->Form->postLink($voteup_title,$voteup_url,array('class'=>'btn btn-default '.$voteup_class,'escape'=>false));
	?>
</div>