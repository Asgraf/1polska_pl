<?php
/**
 * @var $this View
 * @var $representative
 */
$voteup_class = $votedown_class = '';
$div_title = 'Ten reprezentant jest Ci obojętny/neutralny. Brak Twojego głosu poparcia lub sprzeciwu.';
$value = AuthComponent::user('RepresentativesUser.'.$representative['Representative']['id'].'.value');

$voteup_url = $votedown_url = array(
	'plugin'=>false,
	'admin'=>false,
	'controller'=>'representatives',
	'action'=>'cancelvote',
	'id'=>$representative['Representative']['id']
);

$voteup_title = Hash::get($representative,'Representative.upvotes').' '.$this->Html->image('thumb_up.png');
$votedown_title = Hash::get($representative,'Representative.downvotes').' '.$this->Html->image('thumb_down.png');

if($value==1) {
	$voteup_class = 'active';
	$div_title = 'Popierasz tego reprezentanta';
	$voteup_title = Hash::get($representative,'Representative.upvotes').' '.$this->Html->image('thumb_up2.png');
} else {
	$voteup_url['action']='upvote';
}
if($value==-1) {
	$votedown_class = 'active';
	$div_title = 'Nie popierasz tego reprezentanta';
	$votedown_title = Hash::get($representative,'Representative.downvotes').' '.$this->Html->image('thumb_down2.png');
} else {
	$votedown_url['action']='downvote';
}

?>
<div id="vote_widget_representative_<?=$representative['Representative']['id']?>" title="<?=$div_title?>" class="representative_vote btn-group">
	<?php
	echo '<div class="btn btn-default btn-large default_cursor">'.round(Hash::get($representative,'Representative.vote_rate')*100).'%</div>';
	echo $this->Form->postLink($voteup_title,$voteup_url,array('class'=>'btn btn-large btn-success '.$voteup_class,'escape'=>false));
	echo $this->Form->postLink($votedown_title,$votedown_url,array('class'=>'btn btn-large btn-danger '.$votedown_class,'escape'=>false));
	?>
</div>