<?php
/**
 * @var $this View
 */
$this->Html->script('https://www.google.com/recaptcha/api.js',array('block'=>'script'));
?>
<div class="form-group">
	<label class="col col-md-3 control-label">Zaznacz kratkę obok</label>
	<div class="col col-md-9 required">
		<div class="g-recaptcha" data-sitekey="<?=Configure::read('Recaptcha.site_key');?>"></div>
	</div>
</div>