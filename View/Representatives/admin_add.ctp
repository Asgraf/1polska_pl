<?php
/**
 * @var $this View
 */
$this->set('title_for_layout','Nowy Reprezentant');
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="representatives form">
	<?php echo $this->Form->create('Representative',array('role'=>'form','type'=>'file')); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('Reprezentant');?></legend>
		<?php
		echo $this->Form->input('firstname',array('','label'=>__('Imię'),'maxlength'=>20));
		echo $this->Form->input('lastname',array('label'=>__('Nazwisko'),'maxlength'=>20));
		echo $this->Form->input('photo',array('type'=>'file','label'=>__('Zdjęcie')));
		echo $this->Form->input('description',array('label'=>__('Krótki opis'),'maxlength'=>160));
		echo $this->Form->input('content',array('label'=>__('Pełny opis'),'data-markdown-editor'));
		echo $this->Form->input('status',['options'=>['not_active'=>'niewidoczny','active'=>'widoczny','deleted'=>'zablokowany']]);
		echo $this->Form->input('Tag',['label'=>'Tagi']);
		echo $this->element('recaptcha');
		?>
	</fieldset>
	<?php echo $this->Form->button(__('Wyślij'),array('class'=>'btn btn-default submit','data-icon'=>'ok')); ?>
	<?php echo $this->Form->end(); ?>
</div>
