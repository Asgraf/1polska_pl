<?php
/**
 * @var $this View
 */
$this->set('title_for_layout','Reprezentanci');
$width = $height = 60;
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>

<section class="representatives index">
	<?php if(empty($representatives)) {
		if(!$this->request['requested']) echo __('No items to show');
	} else {
	?>
	<table class="table">
		<thead>
		<tr>
			<th class="name"><?php
				echo !empty($all)?__('Imię'):$this->Paginator->sort('firstname',__('Imię'));
				echo '&nbsp;';
				echo !empty($all)?__('Nazwisko'):$this->Paginator->sort('lastname',__('Nazwisko'));
				?></th>
			<th class="description"><?=__('Krótki opis');?></th>
			<?php if(!$this->request->query('status')) { ?>
				<th class="status"><?php echo !empty($all)?__('Status'):$this->Paginator->sort('status',__('Status')); ?></th>
			<?php } ?>

			<?php if(!$this->request->query('created')) { ?>
				<th class="created"><?php echo !empty($all)?__('Dodano'):$this->Paginator->sort('created',__('Dodano')); ?></th>
			<?php } ?>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($representatives as $representative) { ?>
			<tr>
				<td class="name">
				<?=$this->Html->link($representative['Representative']['name'],array('action'=>'edit','slug'=>slug($representative['Representative']['name']),'id'=>$representative['Representative']['id']),array('class'=>'edit'));?>
				</td>
				<td class="description"><?php echo $this->Format->string($representative['Representative']['description']); ?></td>
				<?php if(!$this->request->query('status')) { ?>
					<td class="status"><?php echo $representative['Representative']['status']; ?></td>
				<?php } ?>
				<?php if(!$this->request->query('created')) { ?>
					<td class="created"><?php echo $this->Format->datetime($representative['Representative']['created']); ?></td>
				<?php } ?>

			</tr>
		<?php }?>
		</tbody>
	</table>
	<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
</section>
<?php }?>
<nav class="actions">
	<?php echo $this->Html->link(__('Dodaj'),array('action'=>'add','?'=>$this->request->query),array('class'=>'add btn btn-default','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
