<?php
/**
 * @var $this View
 */
$this->set('title_for_layout','Reprezentanci');
$width = $height = 60;
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>

<?php if($this->request['status']=='active') { ?>
		<div style="margin-top:10px; margin-bottom:12px; margin-left:28px; margin-right:58px; padding:5px 25px; background-color:#DDEEDD; border-color:#999999; border-radius:20px">
		<h4><strong style="color:#00AA00">Uwaga! Na liście reprezentantów znajdują się wyłącznie osoby, które popierają tę inicjatywę (same się zgłosiły lub zgodziły się być na tej liście) - ZAPRASZAM wszystkich dobrych ludzi do przyłączenia się.</strong></h4>
<h5 style="color:#669966">Reprezentantów zaproponowanych samowolnie przez użytkowników można znaleźć <a href="http://1polska.pl/reprezentanci/propozycje_uzytkownikow">TUTAJ</a> - przekonajmy te osoby, żeby włączyły się w inicjatywę zjednoczenia polskiego społeczeństwa. <a href="https://www.youtube.com/watch?v=D-Rq0hRldbE">ZAPRASZAM!</a></h5>
</div>
<?php }?>

<section class="representatives index">
	<?php if(empty($representatives)) {
		if(!$this->request['requested']) echo __('No items to show');
	} else {
		if($this->request['status']=='active') {
			echo '<h2>'.__('Reprezentanci i liderzy:<br>(Komu ufasz? Kto Ci nie pasuje? - szukamy uczciwych osób, godnych zaufania)').'</h2>';
		} else {
			echo '<h2>'.__('Propozycje użytkowników:').'</h2>';
		}
	?>
	<table class="table">
		<thead>
		<tr>
			<th class="photo"></th>
			<th class="name"><?php
				echo !empty($all)?__('Imię'):$this->Paginator->sort('firstname',__('Imię'));
				echo '&nbsp;';
				echo !empty($all)?__('Nazwisko'):$this->Paginator->sort('lastname',__('Nazwisko'));
				?></th>
			<th class="description"><?=__('Krótki opis');?></th>
			<th class="vote_rate"><?php echo !empty($all)?__('Zaufanie'):$this->Paginator->sort('vote_rate',__('Poparcie')); ?></th>
			<th class="upvotes"><?php echo !empty($all)?__('Zaufanie'):$this->Paginator->sort('upvotes',__('Zaufanie')); ?></th>
			<th class="downvotes"><?php echo !empty($all)?__('Brak Zaufania'):$this->Paginator->sort('downvotes',__('Brak Zaufania')); ?></th>
			<?php if(!$this->request->query('created')) { ?>
				<th class="created"><?php echo !empty($all)?__('Dodano'):$this->Paginator->sort('created',__('Dodano')); ?></th>
			<?php } ?>
		</tr>
		</thead>
		<tbody>
		<?php
		foreach ($representatives as $representative) {
			$connected_sign = Hash::get($representative,'ConnectedUser.status')=='active'?'<span class="text-success glyphicon glyphicon-ok" aria-hidden="true" title="Ten reprezentant posiada aktywne konto użytkownika na 1polska.pl"></span> ':'';
			?>
			<tr>
				<td class="photo"><?php
					if($representative['Photo']['id']) {
						echo $this->Html->link(
							$this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$representative['Photo']['id'],'access_key'=>$representative['Photo']['access_key'],$width,$height,1),array('class'=>'img-rounded','width'=>$width,'height'=>$height)),
							array('action'=>'view','slug'=>slug($representative['Representative']['name']),'id'=>$representative['Representative']['id']),
							array('escape'=>false)
						);
					} else {
						echo $this->Html->link(
							$this->Html->image('anonim.jpg',array('class'=>'img-rounded','width'=>$width,'height'=>$height)),
							array('action'=>'view','slug'=>slug($representative['Representative']['name']),'id'=>$representative['Representative']['id']),
							array('escape'=>false)
						);
					}
				?></td>
				<td class="name">
				<?=$connected_sign.$this->Html->link($representative['Representative']['name'],array('action'=>'view','slug'=>slug($representative['Representative']['name']),'id'=>$representative['Representative']['id']),array('class'=>'view visitable'));?>
				</td>
				<td class="description"><?php echo $this->Format->string($representative['Representative']['description']); ?></td>
				<td class="votes text-center" colspan="3"><?=$this->element('representative_vote',array('representative'=>$representative))?></td>
				<?php if(!$this->request->query('created')) { ?>
					<td class="created"><?php echo $this->Format->datetime($representative['Representative']['created']); ?></td>
				<?php } ?>
			</tr>
		<?php }?>
		</tbody>
	</table>
	<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
</section>
<?php }?>
<nav class="actions">
	<?php echo $this->Html->link(__('Zaproponuj SIEBIE na reprezentanta'),array('action'=>'add','?'=>$this->request->query),array('class'=>'add btn btn-default','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
