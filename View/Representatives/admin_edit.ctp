<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="representatives form">
<?php echo $this->Form->create('Representative',array('role'=>'form','type'=>'file')); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('Reprezentant');?></legend>
	<?php
	if($this->Form->value('Representative.connected_user_id')) {
		echo $this->Html->link('Ten reprezentant już jest powiązany z kontem użytkownika',['controller'=>'users','action'=>'edit','id'=>$this->Form->value('Representative.connected_user_id')]);
	} else {
		echo 'Ten reprezentant nie jest powiązany z żadnym kontem użytkownika';
	}
	echo $this->Form->input('firstname',array('','label'=>__('Imię'),'maxlength'=>20));
	echo $this->Form->input('lastname',array('label'=>__('Nazwisko'),'maxlength'=>20));
	echo $this->Form->input('photo',array('type'=>'file','label'=>__('Zdjęcie')));
	echo $this->Form->input('description',array('label'=>__('Krótki opis'),'maxlength'=>160));
	echo $this->Form->input('content',array('label'=>__('Treść'),'data-markdown-editor'));
	echo $this->Form->input('status',['options'=>['not_active'=>'niewidoczny','active'=>'widoczny','deleted'=>'zablokowany']]);
	echo $this->Form->input('Tag',['label'=>'Tagi']);
	echo $this->element('recaptcha');
	?>
	</fieldset>
<?php
echo $this->Form->button(__('Submit'),array('class'=>'submit','data-icon'=>'ok'));
echo $this->Form->end();
?>
</div>