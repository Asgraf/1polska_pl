<?php
/**
 * @noinspection PhpUndefinedClassInspection
 * @var View $this
 */
?>
<nav class="pagination">
<?php
if($this->Helpers->loaded('Paginator') && $this->Paginator->hasPage(2)) {
	$url = requestUrl();
	echo $this->Paginator->prev('< '.__d('cake','previous'),array('url'=>$url),null,array('class'=>'prev disabled'));
	echo $this->Paginator->numbers(array('separator'=>'','url'=>$url));
	echo $this->Paginator->next(__d('cake','next').' >',array('url'=>$url),null,array('class'=>'next disabled'));
}
?>
</nav>