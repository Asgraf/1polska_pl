<?php
return;
/**
 * @noinspection PhpUndefinedClassInspection
 * @var View $this
 */
if($this->request['requested']) return;
if($this->request['ext']=='ajax') return;
?>
<div id="searchbox" class="search form">
<?php
echo $this->Form->create(null,array('action'=>'index','type'=>'get'));
echo $this->Form->search('q',array('value'=>$this->request->query('q')));
echo $this->Form->button(__('Szukaj'), array('class'=>'btn btn-default inline','type'=>'submit','data-role'=>'button','data-icon'=>'search','data-inline'=>'true'));
echo $this->Form->end();
?>
</div>