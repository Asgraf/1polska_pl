<?php
$this->set('title_for_layout','Kontakt');
?>

<h2 class="kontakt">Centrum informacyjno-organizacyjno-koordynacyjne:</h2>
<p class="o_co_chodzi">
Kontakt dla osób chcących działać "w realu" (nie tylko w sieci) oraz lokalnie na rzecz zjednoczenia społeczeństwa i połączenia sił:<br>
<strong>Paweł Skórnicki</strong> (<a href="mailto:powstaniepolskie@wp.pl?Subject=1Polska%20stronaWWW">powstaniepolskie@wp.pl</a>) - koordynator spraw organizacyjnych i kontakt informacyjny<br>
</p>

<iframe src="https://docs.google.com/document/d/1a9gZ_MiNQe4OvUhJ6GXf2BR3l1XpawJzt_nY_Jy3TOw/pub?embedded=true" width="1000" height="1200" style="border:none"></iframe>

<h2 class="kontakt">Administratorzy strony:</h2>
<p class="o_co_chodzi">
Kontakt w sprawach technicznych:<br>
<strong>Michał Turek</strong> (<a href="mailto:asgraf+1polska@gmail.com?Subject=1Polska%20stronaWWW">asgraf+1polska@gmail.com</a>) - główny programista i autor kodu strony<br>
</p>
<p class="o_co_chodzi">
Kontakt w sprawach merytorycznych oraz <strong>ZGŁOSZENIA REPREZENTANTÓW</strong>:<br>
<strong>Konrad Daniel</strong> (<a href="mailto:podziemnatv@gmail.com?Subject=1Polska%20stronaWWW">podziemnatv@gmail.com</a>) - pomysłodawca tego narzędzia internetowego
</p>
<p class="o_co_chodzi">

</p>

<h2 class="kontakt">Dla programistów i grafików chętnych pomóc w doskonaleniu strony 1Polska:</h2>

<p class="o_co_chodzi">
Kod źródłowy tej strony przeznaczony jest do upublicznienia na zasadach OpenSource.
<br>Repozytorium znajduje się: <a href="https://bitbucket.org/Asgraf/1polska_pl/">TUTAJ</a>
<br>
</p>

<h2 class="kontakt">FAQ - najczęściej zadawane pytania:</h2>

<p class="o_co_chodzi">
<strong style="color:#990000">1. Problem z zalogowaniem przez Facebook?</strong><br>
Ustawienia facebooka prawdopodobnie blokują dostęp do adresu email niezbędnego do utworzenia konta. W takim przypadku proszę wejść na link z ustawieniami: <a href="https://www.facebook.com/settings?tab=applications">https://www.facebook.com/settings?tab=applications</a> - usunąć stamtąd aplikację 1Polska i przy kolejnej próbie logowania na stronę 1Polska proszę NIE BLOKOWAĆ dostępu do adresu email.
</p>


<!--
<h3 class="kontakt">Koordynatorzy regionalni:</h2>
<p>DOLNOŚLĄSKIE</p>
<ul>
  <li><strong>Tomasz Lingo</strong> (<a href="mailto:tomasz.lingo@gmail.com?Subject=1Polska%20kontakt">tomasz.lingo@gmail.com</a>)</li>
</ul>  
<p>LUBELSKIE</p>
<ul>
  <li><strong>Adam Stalerzuk</strong> (<a href="mailto:adwist.homeservice@gmail.com?Subject=1Polska%20kontakt">adwist.homeservice@gmail.com</a>)</li>
</ul>  
<p>MAŁOPOLSKIE</p>
<ul>
  <li><strong>Przemysław Paw</strong> (<a href="mailto:paw.przemyslaw@gmail.com?Subject=1Polska%20kontakt">paw.przemyslaw@gmail.com</a>)</li>
</ul>  
<p>MAZOWIECKIE</p>
<ul>
  <li><strong>Wojciech Edward Dobrzyński</strong> (<a href="mailto:wojdob1@wp.pl?Subject=1Polska%20kontakt">wojdob1@wp.pl</a>)</li>
</ul>  
<p>ŚLĄSKIE</p>
<ul>
  <li><strong>Adam Polak</strong> (<a href="mailto:mail4adas@yahoo.com?Subject=1Polska%20kontakt">mail4adas@yahoo.com</a>)</li>
</ul>  
<p>WARMIŃSKO-MAZURSKIE</p>
<ul>
  <li><strong>Anna Mucha</strong> (<a href="mailto:anna.mucha@hotmail.com?Subject=1Polska%20kontakt">anna.mucha@hotmail.com</a>)</li>
</ul>  
-->


