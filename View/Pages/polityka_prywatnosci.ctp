<?php
$this->set('title_for_layout','Polityka prywatności');
?>
<h2>Polityka prywatności</h2>

<p>
<br>
<strong>1Polska.pl jest prywatną, niekomercyjną stroną o charakterze społecznym - i powstała "pro publico bono".</strong><br>
<br>
<strong>1Polska.pl NIE udostępnia (i nie będzie udostępniać) żadnych danych swoich użytkowników innym serwisom ani podmiotom zewnętrznym.</strong><br>
<br>
Rejestracja/zalogowanie (poprzez formularz na stronie lub poprzez konta społecznościowe: google lub facebook) jest dobrowolna ale konieczna aby w pełni korzystać z funkcjonalności strony. Nie jest wymagana aby przeglądać zawartość strony.<br>
<br>
Podczas rejestracji użytkownika (utworzenia konta) strona 1Polska.pl potrzebuje i gromadzi następujące dane:<br>
- Adres email,<br>
- Nazwę użytkownika (wpisaną w formularzu rejestracyjnym lub powiązaną z kontem społecznościowym z którego nastąpiło logowanie),<br>
- Avatar (zdjęcie) użytkownika powiązane z kontem na Google, Facebook'u, lub Gravatar - w zależności od sposobu rejestracji/logowania.<br>
<br>
Użytkownicy mają prawo i możliwość usunięcia swojego konta wraz ze swoimi danymi - jeśli zechcą tak zrobić.<br>
<br>
<strong>Adresy email użytkowników są poufne</strong> (wyłącznie do wiadomości administratorów) i nie są nigdzie na stronie publikowane ani widoczne.<br>
(Jawna jest natomiast domena konta email użytkownika, czyli to co w adresie email znajduje się po symbolu "@" - wyjaśnienie poniżej)<br>
<br>
Podczas oddania głosu (przyciski "popieram" lub "kciuk w górę" lub "kciuk w dół") przez zalogowanego użytkownika, zapisywany jest adres IP głosującego.<br>
<br>
Na stronie prezentowane są statystyki (historia) głosowań, na których widoczne są następujące dane użytkowników, którzy oddali głos w danym głosowaniu:<br>
- Nazwa użytkownika,<br>
- Avatar (zdjęcie użytkownika),<br>
- Domena, na której użytkownik ma swój adres email (to co znajduje się po symbolu "@" w adresie email),<br>
- Pierwsze 3 cyfry adresu IP (ostatnia, indywidualna część adresu IP jest niewidoczna)<br>
<br>
Jawność powyższych danych głosujących użytkowników ma na celu wykrycie nadużyć i sabotażu lub manipulowania procesu głosowania i działania tej strony poprzez zakładanie/wykorzystanie wielu kont przez te same osoby.<br>
<br>
<strong>Strona 1Polska.pl opiera się na wzajemnym zaufaniu jej użytkowników.<br>
Niedopuszczalne jest tworzenie więcej niż jednego konta użytkownika (multikont) przez jedną i tą samą osobę w celu manipulowania ilością głosów.</strong><br>
<br>
<br>
Strona 1Polska.pl wykorzystuje tzw. widgety innych serwisów internetowych takich jak np.: google, facebook, gravatar - i nie odpowiada w żaden sposób za politykę prywatności ani praktyki tych (zewnętrznych) firm/serwisów internetowych.<br>
Korzysta jedynie z ogólnie dostępnych funkcjonalności społecznościowych tych zewnętrznych serwisów.<br>

</p>