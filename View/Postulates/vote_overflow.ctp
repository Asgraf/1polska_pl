<?php
/**
 * @var $this View
 * @var $title_for_layout
 * @var $description_for_layout
 * @var $representatives
 * @var $postulates
 * @var $users_count
 */
$this->set('title_for_layout','Postulaty');
$this->Html->script('vote_overflow',['block'=>'script'])
?>
	<h1>Lista popieranych przez ciebie postulatów</h1>
	<h4>Pozostaw na tej liście 12 postulatów które uważasz za najważniejsze, pozostałe usuń</h4>
<div class="row">
	<?php
	foreach($postulates as $postulate) {
		$url = ['controller'=>'postulates','action'=>'view','id'=>$postulate['Postulate']['id'],'slug'=>slug($postulate['Postulate']['name'])];
		echo '<div class="col-md-4 h210 vote_block">';
		echo '<div class="w140 truncate" data-click-url="'.$this->Html->url($url).'">';
		echo '<h3>'.$this->Html->link($postulate['Postulate']['name'],$url).'</h3>';
		echo $postulate['Postulate']['description'];
		echo '</div>';
		echo $this->Form->postLink('Usuń',['controller'=>'postulates','action'=>'cancelvote','id'=>$postulate['Postulate']['id']],['class'=>'btn btn-danger postulate_vote postulate_vote_overflow']);
		echo '</p>';
		echo '</div>';
	}
	?>
</div>
