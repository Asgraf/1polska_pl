<?php
/**
 * @var $this View
 */
?>
<div class="postulates form">
	<?php echo $this->Form->create('Postulate'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__('Postulat');?></legend>
		<?php
		echo $this->Form->input('name',array('label'=>__('Tytuł'),'maxlength'=>50));
		echo $this->Form->input('description',array('label'=>__('Krótki opis'),'maxlength'=>160));
		echo $this->Form->input('content',array('label'=>__('Treść'),'data-markdown-editor'));
		echo $this->Form->input('status',['options'=>['not_active'=>'niewidoczny','active'=>'widoczny','deleted'=>'zablokowany']]);
		echo $this->Form->input('Tag',['label'=>'Tagi']);
		echo $this->element('recaptcha');
		?>
	</fieldset>
	<?php echo $this->Form->button(__('Wyślij'),array('class'=>'submit','data-icon'=>'ok')); ?>
	<?php echo $this->Form->end(); ?>
</div>