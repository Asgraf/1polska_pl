<?php
/**
 * @var $this View
 */
$this->set('title_for_layout','Postulaty');
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>
<section class="postulates index">
	<?php if(empty($postulates)) {
		if(!$this->request['requested']) echo __('No items to show');
	} else {
	?>
	<table class="table">
		<thead>
		<tr>
			<?php if(!$this->request->query('name')) { ?>
				<th class="name"><?=__('Tytuł')?></th>
			<?php } ?>
			<?php if(!$this->request->query('status')) { ?>
				<th class="status"><?php echo !empty($all)?__('Status'):$this->Paginator->sort('status',__('Status')); ?></th>
			<?php } ?>
			<?php if(!$this->request->query('created')) { ?>
				<th class="created"><?php echo !empty($all)?__('Utworzono'):$this->Paginator->sort('created',__('Utworzono')); ?></th>
			<?php } ?>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($postulates as $postulate) { ?>
			<tr>
				<?php if(!$this->request->query('name')) { ?>
					<td class="name">
						<?php echo $this->Html->link($postulate['Postulate']['name'],array('action'=>'edit','slug'=>slug($postulate['Postulate']['name']),'id'=>$postulate['Postulate']['id']),array('class'=>'edit')); ?>
						<p><?php echo $this->Format->string($postulate['Postulate']['description']); ?></p>
					</td>
				<?php } ?>
				<?php if(!$this->request->query('status')) { ?>
					<td class="status"><?php echo $postulate['Postulate']['status']; ?></td>
				<?php } ?>
				<?php if(!$this->request->query('created')) { ?>
					<td class="created"><?php echo $this->Format->datetime($postulate['Postulate']['created']); ?></td>
				<?php } ?>
			</tr>
		<?php }?>
		</tbody>
	</table>
	<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
</section>
<?php }?>
<nav class="actions">
	<?php echo $this->Html->link(__('Dodaj'),array('action'=>'add','?'=>$this->request->query),array('class'=>'add btn btn-default','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
