<?php
/**
 * @var $this View
 */
$this->set('title_for_layout','Postulaty');
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>

<div style="margin-top:10px; margin-bottom:12px; margin-left:28px; margin-right:58px; padding:5px 25px; background-color:#DDEEDD; border-color:#999999; border-radius:20px">
	<h4><strong style="color:#00AA00">Uwaga! Lista postulatów służy do wyłonienia tego co nas łączy - a nie spisywania wszystkiego co nas dzieli. Dlatego lista będzie regularnie ograniczana do postulatów z największym poparciem - a reszta postulatów będzie regularnie z listy zdejmowana.</strong></h4>
	<h5 style="color:#669966">Postulaty zdjęte z listy można znaleźć <a href="http://1polska.pl/postulaty/propozycje_uzytkownikow">TUTAJ</a> - i te, które zdobędą odpowiednio duże poparcie, pojawią się oczywiście na liście postulatów, zastępując postulaty z końca listy, itd.</h5>
</div>



<section class="postulates index">
	<?php if(empty($postulates)) {
		if(!$this->request['requested']) echo __('No items to show');
	} else {
	echo '<h2>'.__('Postulaty zmian - popieraj te, które akceptujesz - szukamy tego co nas łączy<br>(Ta lista to "wylęgarnia pomysłów" lub jak kto woli: poczekalnia)').'</h2>';
	?>
	<table class="table">
		<thead>
		<tr>
			<?php if(!$this->request->query('name')) { ?>
				<th class="name"><?=__('Tytuł')?></th>
			<?php } ?>
			<th class="upvotes" colspan="2"><?php echo !empty($all)?__('Poparcie'):$this->Paginator->sort('upvotes',__('Poparcie')); ?></th>
			<?php if(!$this->request->query('created')) { ?>
				<th class="created"><?php echo !empty($all)?__('Utworzono'):$this->Paginator->sort('created',__('Utworzono')); ?></th>
			<?php } ?>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($postulates as $postulate) { ?>
			<tr>
				<?php if(!$this->request->query('name')) { ?>
					<td class="name">
						<?php echo $this->Html->link($postulate['Postulate']['name'],array('action'=>'view','slug'=>slug($postulate['Postulate']['name']),'id'=>$postulate['Postulate']['id']),array('class'=>'view visitable')); ?>
						<p><?php echo $this->Format->string($postulate['Postulate']['description']); ?></p>
					</td>
				<?php } ?>
				<td class="upvotes">
					<?=$this->element('postulate_vote',array('postulate'=>$postulate))?>
				</td>
				<td class="upvotes progressbar">
					<?php
					$value = round($postulate['Postulate']['upvotes']/$max_vote_count*100,2);
					?>
					<div class="progress" title="Popiera <?=$postulate['Postulate']['upvotes']?> osób">
						<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$postulate['Postulate']['upvotes']?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$value?>%;"></div>
					</div>
				</td>
				<?php if(!$this->request->query('created')) { ?>
					<td class="created"><?php echo $this->Format->datetime($postulate['Postulate']['created']); ?></td>
				<?php } ?>
			</tr>
		<?php }?>
		</tbody>
	</table>
	<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
</section>
<?php }?>
<nav class="actions">
	<?php echo $this->Html->link(__('Zaproponuj nowy postulat'),array('action'=>'add','?'=>$this->request->query),array('class'=>'add btn btn-default','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
