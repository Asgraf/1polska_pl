<?php
/**
 * @var $this View
 * @var $token
 * @var $email
 */
?>
Witaj

Otrzymujesz ten email ponieważ skorzystano z opcji "Zapomniałem hasła"
Kliknij w poniższy jednorazowy link aby zostać automatycznie zalogowanym na swoje konto na 1Polska.pl
Po zalogowaniu, swoje hasło możesz zmienić w edycji profilu.

<?=$this->Html->link(array('controller'=>'users','action'=>'forgot_password','?'=>array('email'=>$email,'token'=>$token),'full_base'=>true))."\n"?>

Jeżeli to nie ty skorzystałeś z opcji "Zapomniałem hasła" możesz zignorować i usunąć ten email.
Powyższy link wygaśnie automatycznie w ciągu godziny od jego wygenerowania.