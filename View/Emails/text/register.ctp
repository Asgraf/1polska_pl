
<?=__d('user','Welcome %s!',$username)."\n";?>

<?=__d('user','Konto na %s zostało pomyślnie utworzone.',$_SERVER['HTTP_HOST'])."\n";?>
<?=__d('user','Oto twoje dane do logowania poprzez formularz na stronie: (jeśli logowałeś się poprzez swoje konto społecznościowe poniższe dane pozwolą Ci się w razie czego zalogować opcjonalnie poprzez formularz na stronie)')."\n";?>:

<?=__d('user','Email');?>: <?=$email."\n";?>
<?=__d('user','Password');?>: <?=$password."\n";?>

<?=__d('user','Możesz zmienić swoje hasło w swoim %s',__d('user','user profile'));?>:
<?=$this->Html->url(array('controller'=>'users','action'=>'edit'),true);?>.