
<?=__d('user','Welcome %s!',$username);?><br />
<br />
<?=__d('user','Konto na %s zostało pomyślnie utworzone.',$this->Html->link($_SERVER['HTTP_HOST'],array_merge(Router::parse('/'),array('full_base'=>true))));?><br />
<?=__d('user','Oto twoje dane do logowania poprzez formularz na stronie: (jeśli logowałeś się poprzez swoje konto społecznościowe poniższe dane pozwolą Ci się w razie czego zalogować opcjonalnie poprzez formularz na stronie)');?>:<br />
<br />
<?=__d('user','Email');?>: <b><?=$email;?></b><br />
<?=__d('user','Password');?>: <b><?=$password;?></b><br />
<br />
<?=__d('user','Możesz zmienić swoje hasło w swoim %s',$this->Html->link(__d('user','user profile'),array('controller'=>'users','action'=>'edit'),array('full_base'=>true)));?>.

