<?php
/**
 * @var $this View
 * @var $token
 * @var $email
 */
?>
Witaj<br />
<br />
Otrzymujesz ten email ponieważ skorzystano z opcji "Zapomniałem hasła"<br />
Kliknij w poniższy jednorazowy link aby zostać automatycznie zalogowanym na swoje konto na 1Polska.pl<br />
Po zalogowaniu swoje hasło możesz zmienić w edycji profilu.<br />
<br />
<?=$this->Html->link(array('controller'=>'users','action'=>'forgot_password','?'=>array('email'=>$email,'token'=>$token),'full_base'=>true))?><br />
<br />
Jeżeli to nie ty skorzystałeś z opcji "Zapomniałem hasła" możesz zignorować i usunąć ten email.<br />
Powyższy link wygaśnie automatycznie w ciągu godziny od jego wygenerowania.