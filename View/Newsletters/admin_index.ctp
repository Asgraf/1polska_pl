<?php
/**
 * @var $this View
 */
?>
<section class="newsletters index">
	<?php if (empty($newsletters)) {
		if(!$this->request['requested'])
			echo __('No items to show');
	} else {
	?>    <?php echo '<h2>'.__('Newsletters').'</h2>'; ?>
	<table class="table">
		<thead>
		<tr>
			<th class="subject"><?php echo !empty($all) ? __('Subject') : $this->Paginator->sort('subject', __('Tytuł')); ?></th>
			<th class="status"><?php echo !empty($all) ? __('Status') : $this->Paginator->sort('status', __('Status')); ?></th>
			<th><?php echo __('Info'); ?></th>
			<th class="created"><?php echo !empty($all) ? __('Created') : $this->Paginator->sort('created', __('Utworzono')); ?></th>
			<th class="modified"><?php echo !empty($all) ? __('Modified') : $this->Paginator->sort('modified', __('Zmodyfikowano')); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
		</thead>
		<tbody><?php foreach($newsletters as $newsletter) {
			if($newsletter['Newsletter']['status']=='queted' && $newsletter['Newsletter']['sent_num']>0) {
				$newsletter['Newsletter']['status']='sending';
			}
			?>
			<tr<?=$newsletter['Newsletter']['status']=='unsent'?' data-default-url="'.$this->Html->url(array('action'=>'edit', 'id'=>$newsletter['Newsletter']['id'])).'"':''?>>
				<td class="subject"><?php echo $this->Format->string($newsletter['Newsletter']['subject']); ?></td>
				<td class="status"><?php echo $this->Format->enum($newsletter['Newsletter']['status']); ?></td>
				<td><?php
					switch($newsletter['Newsletter']['status']) {
						case 'sent':
							break;
						case 'sending':
							echo '<meter title="'.$newsletter['Newsletter']['sent_num'].'/'.$newsletter['Newsletter']['sent_max'].'" value="'.$newsletter['Newsletter']['sent_num'].'" max="'.$newsletter['Newsletter']['sent_max'].'">'.$newsletter['Newsletter']['sent_num'].'/'.$newsletter['Newsletter']['sent_max'].'</meter>';
							break;
						default:
							if($newsletter['Newsletter']['sent_max']>0) {
								echo __('%s przypisanych użytkowników',$newsletter['Newsletter']['sent_max']);
							} else {
								echo __('Brak przypisanych użytkowników!!!');
								echo ' ';
								echo $this->Form->postLink('Przypisz wszystkich',['action'=>'mass_assign_all_users','id'=>$newsletter['Newsletter']['id'],'slug'=>slug($newsletter['Newsletter']['subject'])]);
							}

					}
					?></td>
				<td class="created"><?php echo $this->Format->datetime($newsletter['Newsletter']['created']); ?></td>
				<td class="modified"><?php echo $this->Format->datetime($newsletter['Newsletter']['modified']); ?></td>
				<td class="actions">
					<?php
					echo $this->Html->link(__('View'), array('action'=>'view', 'slug'=>slug($newsletter['Newsletter']['subject']), 'id'=>$newsletter['Newsletter']['id']), array('class'=>'view btn btn-default')).' ';
					echo $this->Form->postLink(__('Wyślij mi maila testowego'), array('action'=>'send_test_email', 'slug'=>slug($newsletter['Newsletter']['subject']), 'id'=>$newsletter['Newsletter']['id']), array('class'=>'view btn btn-default')).' ';
					if($newsletter['Newsletter']['sent_max']>0 && in_array($newsletter['Newsletter']['status'],['unsent','draft'])) {
						echo $this->Form->postLink(__('Roześij Newslettera'), array('action'=>'edit', 'slug'=>slug($newsletter['Newsletter']['subject']), 'id'=>$newsletter['Newsletter']['id']), array('data'=>array('status'=>'queted'), 'class'=>'email btn btn-success')).' ';
					}
					if(in_array($newsletter['Newsletter']['status'],['unsent','draft'])) {
						echo $this->Html->link(__('Edit'), array('action'=>'edit', 'slug'=>slug($newsletter['Newsletter']['subject']), 'id'=>$newsletter['Newsletter']['id']), array('class'=>'edit btn btn-default')).' ';
					}
					echo $this->Form->postLink(__('Delete'), array('action'=>'delete', 'slug'=>slug($newsletter['Newsletter']['subject']), 'id'=>$newsletter['Newsletter']['id']), array('class'=>'delete btn btn-danger'), __('Are you sure you want to delete %s newsletter?', $newsletter['Newsletter']['id']));
					?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<?php if(empty($all))
		echo $this->element('Asgraf.paginator'); ?>
</section>
<?php } ?>
<nav class="actions">
	<?php echo $this->Html->link(__('New Newsletter'), array('action'=>'add','?'=>$this->request->query), array('class'=>'btn btn-default')); ?>
</nav>
