<?php
/**
 * @var $this View
 * @var $newsletter
 */
if($newsletter['Newsletter']['status']=='queted' && $newsletter['Newsletter']['sent_num']>0) {
	$newsletter['Newsletter']['status']='sending';
}
?>
<div class="newsletters view">
	<h2><?php echo __( 'Newsletter'); ?></h2>
	<dl>
		<dd class="txt"><?=$newsletter['Newsletter']['txt']; ?></dd>
		<dt><?php echo __( 'Status'); ?></dt>
		<dd class="status"><?=$this->Format->enum($newsletter['Newsletter']['status']); ?></dd>
		<dt><?php echo __( 'Postęp'); ?></dt>
		<dd class="progressbar"><?php
			switch($newsletter['Newsletter']['status']) {
				case 'sent':
					break;
				case 'sending':
					echo '<meter title="'.$newsletter['Newsletter']['sent_num'].'/'.$newsletter['Newsletter']['sent_max'].'" value="'.$newsletter['Newsletter']['sent_num'].'" max="'.$newsletter['Newsletter']['sent_max'].'">'.$newsletter['Newsletter']['sent_num'].'/'.$newsletter['Newsletter']['sent_max'].'</meter>';
					break;
				default:
					if($newsletter['Newsletter']['sent_max']>0) {
						echo __('%s przypisanych użytkowników',$newsletter['Newsletter']['sent_max']);
					} else {
						echo __('Brak przypisanych użytkowników!!!');
					}

			}
			?></dd>
		<dt><?php echo __( 'Utworzono'); ?></dt>
		<dd class="created"><?=$this->Format->datetime($newsletter['Newsletter']['created']); ?></dd>
		<dt><?php echo __( 'Zmodyfikowano'); ?></dt>
		<dd class="modified"><?=$this->Format->datetime($newsletter['Newsletter']['modified']); ?></dd>
		<dt><?php echo __( 'Tytuł'); ?></dt>
		<dd class="subject"><?=$this->Format->string($newsletter['Newsletter']['subject']); ?></dd>
		<dt><?php echo __( 'Treść'); ?></dt>

	</dl>
</div>