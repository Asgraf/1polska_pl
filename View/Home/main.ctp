<?php
/**
 * @var $this View
 * @var $title_for_layout
 * @var $description_for_layout
 * @var $representatives
 * @var $postulates
 * @var $users_count
 */
$this->Html->meta(array('property' => 'og:image', 'content' => 'http://lh6.googleusercontent.com/-hiYEyAESO8U/AAAAAAAAAAI/AAAAAAAAAAY/q7yeRBPt_Q4/photo.jpg'), NULL, array('inline' => false));
$this->set('title_for_layout','1Polska.pl - Strona główna');
$width = $height = 80;
$join_button = AuthComponent::user('id')?'':$this->Html->link('Przyłącz się »',['controller'=>'users','action'=>'register'],['class'=>'btn btn-primary btn-lg pull-right']);
$this->append('header','
<div class="jumbotron" style="padding:10px">
	<div class="container">
		<h5 style="font-style:italic; font-weight:normal; color:#99BB99">„Do triumfu zła wystarczy bierność ludzi dobrych” – Edmund Burke</h5>
		<h1>Czas zjednoczyć dobrych ludzi wobec patologii obecnego systemu!</h1>
		<h4>Chcesz żyć w normalnym kraju? Masz dość absurdów IIIRP? Nie jesteś sam!</h4>
		<p style="font-size:15px">To my, polskie społeczeństwo, które ma już dość tego chorego systemu, jaki stworzyły nam tzw. "elity IIIRP". Pookrągłostołowemu, zabetonowanemu układowi partyjnemu i obecnej klasie politycznej już dziękujemy! Dość hipokryzji, obłudy i zakłamania polityków oraz mediów. Tu ustalimy wspólnie priorytety zmian i wreszcie połączymy siły żeby przywrócić w Polsce normalność. To my: normalni, zwykli ludzie chcący żyć uczciwie i godnie jesteśmy w Polsce większością - i czas to sobie uświadomić!</p>
		<h2 class="text-center">'.$this->Html->link('Zobacz więcej »',['controller'=>'pages','action'=>'display','o_co_chodzi'],['class'=>'btn btn-primary btn-lg pull-left']).'<strong style="margin-left:15px; margin-right:15px; font-weight:normal">Jest nas już <span style="color:#CC3344">'.number_format ($users_count,0,',','&nbsp;').'</span> osób!</strong>
		'.$join_button.'
		</h2>
	</div>
</div>
');
?>
<section class="container">
<img src="https://lh6.googleusercontent.com/-hiYEyAESO8U/AAAAAAAAAAI/AAAAAAAAAAY/q7yeRBPt_Q4/photo.jpg" alt="1Polska LOGO" style="width:42px;height:42px;position:fixed;bottom:6px;right:0;">

	<div class="row">
	</div>
	<div class="row">
		<h2><?=$this->Html->link('PRIORYTETY ZMIAN - najpopularniejsze postulaty (to co nas łączy):',['controller'=>'postulates','action'=>'index','status'=>'active'])?></h2>
		<?php
		foreach($postulates as $postulate) {
			$url = ['controller'=>'postulates','action'=>'view','id'=>$postulate['Postulate']['id'],'slug'=>slug($postulate['Postulate']['name'])];
			echo '<div class="col-md-4 h210">';
			echo '<div class="w140 truncate" data-click-url="'.$this->Html->url($url).'">';
			echo '<h3>'.$this->Html->link($postulate['Postulate']['name'],$url).'</h3>';
			echo $postulate['Postulate']['description'];
			echo '</div>';
			echo '<p class="clearfix">';
			echo $this->element('postulate_vote',array('postulate'=>$postulate)).'&nbsp;';
			echo '</p>';
			echo '</div>';
		}
		?>
		<div><?=$this->Html->link('Zobacz propozycje użytkowników',['controller'=>'postulates','action'=>'index','status'=>'active'],['class'=>'btn btn-primary']);?></div>
	</div>

 <div class="row">
  <h2>LISTA SPOŁECZNA - co to takiego? (jak połączyć siły aby zmienić nasz kraj):</h2>
  <iframe src="https://docs.google.com/presentation/d/14gx2DUk-6XOd4p4TTQdhQJRuedCelurnQOc45IuTmTs/embed?start=true&loop=true&delayms=5000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
 </div>

    <div class="row">
		<h2><?=$this->Html->link('REPREZENTANCI i liderzy - osoby popierające tę inicjatywę:',['controller'=>'representatives','action'=>'index'])?></h2>
		<div style="margin-top:10px; margin-bottom:12px; margin-left:28px; margin-right:58px; padding:5px 25px; background-color:#DDEEDD; border-color:#999999; border-radius:20px">
		<h4><strong style="color:#00AA00">Uwaga! Na liście reprezentantów znajdują się wyłącznie osoby, które popierają tę inicjatywę (same się zgłosiły lub zgodziły się być na tej liście) - ZAPRASZAM wszystkich dobrych ludzi do przyłączenia się.</strong></h4>
<h5 style="color:#669966">Reprezentantów zaproponowanych samowolnie przez użytkowników można znaleźć <a href="http://1polska.pl/reprezentanci/propozycje_uzytkownikow">TUTAJ</a> - przekonajmy te osoby, żeby włączyły się w inicjatywę zjednoczenia polskiego społeczeństwa. <a href="https://www.youtube.com/watch?v=D-Rq0hRldbE">ZAPRASZAM!</a></h5>
</div>
		<?php
		foreach($representatives as $representative) {
			$url = ['controller'=>'representatives','action'=>'view','id'=>$representative['Representative']['id'],'slug'=>slug($representative['Representative']['name'])];
			echo '<div class="col-md-4 h210">';
			echo '<div class="max140 truncate" data-click-url="'.$this->Html->url($url).'">';
			if($representative['Photo']['id']) {
				echo $this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$representative['Photo']['id'],'access_key'=>$representative['Photo']['access_key'],$width,$height,1),array('class'=>'pull-left img-circle','width'=>$width,'height'=>$height));
			} else {
				echo $this->Html->image('anonim.jpg',array('class'=>'pull-left img-circle','width'=>$width,'height'=>$height));
			}
			echo '<h3>'.$this->Html->link($representative['Representative']['name'],$url).'</h3>';
			echo $representative['Representative']['description'];
			echo '</div>';
			echo '<p class="clearfix">';
			echo $this->element('representative_vote',array('representative'=>$representative)).'&nbsp;';
			echo '</p>';
			echo '</div>';
		}
		?>
		<div><?=$this->Html->link('Zobacz wszystkich reprezentantów',['controller'=>'representatives','action'=>'index','status'=>'active'],['class'=>'btn btn-primary']);?></div>
	</div>

 <div class="row">
  <h2>REPREZENTANCI w Twoim OKRĘGU WYBORCZYM (kliknij na swój okręg wyborczy):</h2>
<img src="https://lh4.googleusercontent.com/REhJ_m0A_SJtGVdX1OWKkeULnMfDpxuOmfrZEhWsm9b11epp0MFyhEJkMIdOnBcKsPlI3COhaK3xsxvcvK0e3V4Yady3pCOo-zKb44WAfHJllSNXoLjr21QgP87ru8Rz4nwlZYc" alt="Okręgi wyborcze do Sejmu RP" usemap="#okregi" style="width:512px;height:487px;">

<map name="okregi">
  <area shape="circle" coords="80,306,34" alt="Okr 1 Legnica" title="Okr 1 Legnica" href="/tagi/Okr_01_Legnica" target="_top">
  <area shape="circle" coords="125,360,24" alt="Okr 2 Wałbrzych" title="Okr 2 Wałbrzych" href="/tagi/Okr_02_Wa%C5%82brzych" target="_top">
  <area shape="circle" coords="148,309,30" alt="Okr 3 Wrocław" title="Okr 3 Wrocław" href="/tagi/Okr_03_Wroc%C5%82aw" target="_top">
  <area shape="rect" coords="167,113,210,192" alt="Okr 4 Bydgoszcz" title="Okr 4 Bydgoszcz" href="/tagi/Okr_04_Bydgoszcz" target="_top">
  <area shape="circle" coords="245,162,33" alt="Okr 5 Toruń" title="Okr 5 Toruń" href="/tagi/Okr_05_Toru%C5%84" target="_top">
  <area shape="circle" coords="417,301,33" alt="Okr 6 Lublin" title="Okr 6 Lublin" href="/tagi/Okr_06_Lublin" target="_top">
  <area shape="rect" coords="449,222,502,368" alt="Okr 7 Chełm" title="Okr 7 Chełm" href="/tagi/Okr_07_Che%C5%82m" target="_top">
  <area shape="rect" coords="30,178,92,270" alt="Okr 8 Zielona Góra" title="Okr 8 Zielona Góra" href="/tagi/Okr_08_Zielona_G%C3%B3ra" target="_top">
  <area shape="circle" coords="277,263,14" alt="Okr 9 Łódź" title="Okr 9 Łódź" href="/tagi/Okr_09_%C5%81%C3%B3d%C5%BA" target="_top">
  <area shape="circle" coords="281,300,26" alt="Okr 10 Piotrków Trybunalski" title="Okr 10 Piotrków Trybunalski" href="/tagi/Okr_10_Piotrk%C3%B3w_Trybunalski" target="_top">
  <area shape="circle" coords="239,273,23" alt="Okr 11 Sieradz" title="Okr 11 Sieradz" href="/tagi/Okr_11_Sieradz" target="_top">
  <area shape="circle" coords="263,235,18" alt="Okr 11 Sieradz" title="Okr 11 Sieradz" href="/tagi/Okr_11_Sieradz" target="_top">
  <area shape="circle" coords="281,420,18" alt="Okr 12 Kraków I" title="Okr 12 Kraków I" href="/tagi/Okr_12_Krak%C3%B3w" target="_top">
  <area shape="circle" coords="292,387,18" alt="Okr 13 Kraków II" title="Okr 13 Kraków II" href="/tagi/Okr_13_Krak%C3%B3w" target="_top">
  <area shape="circle" coords="327,460,38" alt="Okr 14 Nowy Sącz" title="Okr 14 Nowy Sącz" href="/tagi/Okr_14_Nowy_S%C4%85cz" target="_top">
  <area shape="circle" coords="333,401,24" alt="Okr 15 Tarnów" title="Okr 15 Tarnów" href="/tagi/Okr_15_Tarn%C3%B3w" target="_top">
  <area shape="rect" coords="280,152,340,187" alt="Okr 16 Płock" title="Okr 16 Płock" href="/tagi/Okr_16_P%C5%82ock" target="_top">
  <area shape="rect" coords="267,187,312,223" alt="Okr 16 Płock" title="Okr 16 Płock" href="/tagi/Okr_16_P%C5%82ock" target="_top">
  <area shape="circle" coords="354,283,32" alt="Okr 17 Radom" title="Okr 17 Radom" href="/tagi/Okr_17_Radom" target="_top">
  <area shape="circle" coords="405,210,32" alt="Okr 18 Siedlce" title="Okr 18 Siedlce" href="/tagi/Okr_18_Siedlce" target="_top">
  <area shape="rect" coords="348,142,384,186" alt="Okr 18 Siedlce" title="Okr 18 Siedlce" href="/tagi/Okr_18_Siedlce" target="_top">
  <area shape="circle" coords="346,223,12" alt="Okr 19 Warszawa I" title="Okr 19 Warszawa I" href="/tagi/Okr_19_Warszawa" target="_top">
  <area shape="circle" coords="344,220,31" alt="Okr 20 Warszawa II" title="Okr 20 Warszawa II" href="/tagi/Okr_20_Warszawa" target="_top">
  <area shape="circle" coords="189,352,32" alt="Okr 21 Opole" title="Okr 21 Opole" href="/tagi/Okr_21_Opole" target="_top">
  <area shape="circle" coords="450,442,54" alt="Okr 22 Krosno" title="Okr 22 Krosno" href="/tagi/Okr_22_Krosno" target="_top">
  <area shape="circle" coords="390,385,32" alt="Okr 23 Rzeszów" title="Okr 23 Rzeszów" href="/tagi/Okr_23_Rzesz%C3%B3w" target="_top">
  <area shape="circle" coords="435,139,45" alt="Okr 24 Białystok" title="Okr 24 Białystok" href="/tagi/Okr_24_Bia%C5%82ystok" target="_top">
  <area shape="rect" coords="210,50,256,110" alt="Okr 25 Gdańsk" title="Okr 25 Gdańsk" href="/tagi/Okr_25_Gda%C5%84sk" target="_top">
  <area shape="rect" coords="144,24,206,100" alt="Okr 26 Gdynia" title="Okr 26 Gdynia" href="/tagi/Okr_26_Gdynia" target="_top">
  <area shape="circle" coords="237,437,28" alt="Okr 27 Bielsko-Biała" title="Okr 27 Bielsko-Biała" href="/tagi/Okr_27_Bielsko-Bia%C5%82a" target="_top">
  <area shape="circle" coords="250,336,22" alt="Okr 28 Częstochowa" title="Okr 28 Częstochowa" href="/tagi/Okr_28_Cz%C4%99stochowa" target="_top">
  <area shape="circle" coords="229,372,14" alt="Okr 29 Gliwice" title="Okr 29 Gliwice" href="/tagi/Okr_29_Gliwice" target="_top">
  <area shape="circle" coords="215,400,16" alt="Okr 30 Rybnik" title="Okr 30 Rybnik" href="/tagi/Okr_30_Rybnik" target="_top">
  <area shape="circle" coords="248,388,11" alt="Okr 31 Katowice" title="Okr 31 Katowice" href="/tagi/Okr_31_Katowice" target="_top">
  <area shape="circle" coords="266,373,15" alt="Okr 32 Sosnowiec" title="Okr 32 Sosnowiec" href="/tagi/Okr_32_Sosnowiec" target="_top">
  <area shape="circle" coords="333,345,38" alt="Okr 33 Kielce" title="Okr 33 Kielce" href="/tagi/Okr_33_Kielce" target="_top">
  <area shape="rect" coords="260,52,308,128" alt="Okr 34 Elbląg" title="Okr 34 Elbląg" href="/tagi/Okr_34_Elbl%C4%85g" target="_top">
  <area shape="circle" coords="361,91,42" alt="Okr 35 Olsztyn" title="Okr 35 Olsztyn" href="/tagi/Okr_35_Olsztyn" target="_top">
  <area shape="rect" coords="125,245,216,280" alt="Okr 36 Kalisz" title="Okr 36 Kalisz" href="/tagi/Okr_36_Kalisz" target="_top">
  <area shape="rect" coords="170,198,230,242" alt="Okr 37 Konin" title="Okr 37 Konin" href="/tagi/Okr_37_Konin" target="_top">
  <area shape="rect" coords="98,150,166,188" alt="Okr 38 Piła" title="Okr 38 Piła" href="/tagi/Okr_38_Pi%C5%82a" target="_top">
  <area shape="rect" coords="95,188,122,237" alt="Okr 38 Piła" title="Okr 38 Piła" href="/tagi/Okr_38_Pi%C5%82a" target="_top">
  <area shape="circle" coords="144,211,20" alt="Okr 39 Poznań" title="Okr 39 Poznań" href="/tagi/Okr_39_Pozna%C5%84" target="_top">
  <area shape="rect" coords="80,50,138,145" alt="Okr 40 Koszalin" title="Okr 40 Koszalin" href="/tagi/Okr_40_Koszalin" target="_top">
  <area shape="rect" coords="20,73,79,166" alt="Okr 41 Szczecin" title="Okr 41 Szczecin" href="/tagi/Okr_41_Szczecin" target="_top">

</map>

 </div>



</section>
