<?php
/**
 * @var $this View
 * @var $tags
 */
?>
	<section class="tags index">
		<h2>Tagi:</h2>
		<!--<canvas id="wordcloud" width="640" height="480"></canvas>-->
		<ul>
			<?php
			//$wordcloud_data = '';
			foreach ($tags as $tag_name=>$tag_count) {
				//$wordcloud_data.=$tag_count.' '.$tag_name.'\n';
				echo '<li data-count="'.$tag_count.'">'.$this->Html->link($tag_name,array('action'=>'view','tag'=>$tag_name)).'</li>';
			}
			//$wordcloud_data = json_encode($wordcloud_data);
			?>
		</ul>

		<script>

		</script>
	</section>
<?php
/*$this->Html->script('wordcloud2',['block'=>'script']);
$this->Html->scriptBlock(<<<JS
$(function(){
WordCloud($('#wordcloud')[0],{
			list: ($wordcloud_data),
			click: function(item, dimension, e){
				console.log(item,dimension);
			}
		});
});
JS
	,['block'=>'script']);*/