<?php
/**
 * @var $this View
 */
?>
<div class="tags view">
	<h2><?php  echo __d('default','Tag').' '.h($tag['Tag']['name']); ?></h2>
	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Reprezentanci z tagiem <b><?=h($tag['Tag']['name'])?></b>:</h3>
				<ul style="-webkit-column-width: 300px;-moz-column-width: 300px;column-width: 300px;">
					<?php
					$count = 0;
					foreach($tag['Representative'] as $representative) {
						$url = ['controller'=>'representatives','action'=>'view','id'=>$representative['id'],'slug'=>slug($representative['name'])];
						echo '<li class=""><a href="'.$this->Html->url($url).'">';
						if(!empty($representative['Photo']['id'])) {
							echo $this->Html->image(array('ext'=>'jpeg','plugin'=>'filerepo','controller'=>'fileobjects','action'=>'thumb','id'=>$representative['Photo']['id'],'access_key'=>$representative['Photo']['access_key'],16,16,1),array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
						} else {
							echo $this->Html->image('anonim.jpg',array('class'=>'pull-left img-circle','width'=>16,'height'=>16));
						}
						echo '&nbsp;';
						echo $representative['name'];
						echo '</a></li>';
						$count++;
					}
					if(!$count) {
						echo '<li>Brak</li>';
					}
					?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3>Postulaty z tagiem <b><?=h($tag['Tag']['name'])?></b>:</h3>
				<ul style="-webkit-column-width: 300px;-moz-column-width: 300px;column-width: 300px;">
					<?php
					$count = 0;
					foreach($tag['Postulate'] as $postulate) {
						$url = ['controller'=>'postulates','action'=>'view','id'=>$postulate['id'],'slug'=>slug($postulate['name'])];
						echo '<li class=""><a href="'.$this->Html->url($url).'">';
						echo $postulate['name'];
						echo '</a></li>';
						$count++;
					}
					if(!$count) {
						echo '<li>Brak</li>';
					}
					?>
				</ul>
			</div>
		</div>
	</section>
</div>

