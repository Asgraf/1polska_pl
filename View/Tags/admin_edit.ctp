<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<div class="tags form">
<?php echo $this->Form->create('Tag'); ?>
	<fieldset id="form" class="form_page">
		<legend><?=__d('default','Tag');?></legend>
	<?php
		echo $this->Form->input('name',array('label'=>__d('default','Name')));
	?>
	</fieldset>
	<?php
		echo $this->Form->input('Postulate',array('label'=>__d('default','Postulate')));
		echo $this->Form->input('Representative',array('label'=>__d('default','Representative')));
	?>
<?php echo $this->Form->button(__d('default','Submit'),array('class'=>'submit','data-icon'=>'ok')); ?>
<?php echo $this->Form->end(); ?>
</div>
<nav class="actions">
	<ul>
		<li><?php echo $this->Form->postLink(__('Delete'),array('action'=>'delete','slug'=>slug($this->Form->value('Tag.name')),'id'=>$this->Form->value('Tag.id')),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-icon'=>'trash'),__d('default','Are you sure you want to delete %s tag?',$this->Form->value('Tag.name'))); ?></li>
		<li><?php echo $this->Html->link(__d('default','List Tags'),array('action'=>'index'),array('class'=>'index','data-role'=>'button','data-inline'=>'true','data-icon'=>'list-ol')); ?></li>
	</ul>
</nav>
