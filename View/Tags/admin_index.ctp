<?php
/**
 * @var $this View
 */
?>
<?php echo $this->element('Asgraf.related_buttons'); ?>
<?php echo $this->element('Asgraf.searchbox'); ?>
<section class="tags index">
	<?php if(empty($tags)) {
		 if(!$this->request['requested']) echo __('No items to show');
	} else {
		echo '<h2>'.__d('default','Tags').'</h2>';
?>
<table class="table">
	<thead>
	<tr>
		<?php if(!$this->request->query('name')) { ?>
		<th class="name"><?php echo !empty($all)?__d('default','Tag'):$this->Paginator->sort('name',__d('default','Tag')); ?></th>
		<?php } ?>
		<th class="actions"><?php echo __d('default','Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($tags as $tag) { ?>
	<tr>
		<?php if(!$this->request->query('name')) { ?>
		<td class="name">
			<?php echo $this->Html->link($tag['Tag']['name'],array('action'=>'edit','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'])); ?>
		</td>
		<?php } ?>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'),array('action'=>'edit','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'],'?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'edit','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'edit')); ?>
			<?php echo $this->Form->postLink(__('Delete'),array('action'=>'delete','slug'=>slug($tag['Tag']['name']),'id'=>$tag['Tag']['id'],'?'=>array('redirect'=>$_SERVER['REQUEST_URI'])),array('class'=>'delete','data-role'=>'button','data-inline'=>'true','data-iconpos'=>'notext','data-icon'=>'trash'),__d('default','Are you sure you want to delete %s tag?',$tag['Tag']['name'])); ?>
		</td>
	</tr>
<?php }?>
	</tbody>
</table>
<?php if(empty($all)) echo $this->element('Asgraf.paginator'); ?>
</section>
<?php }?>
<nav class="actions">
	<?php echo $this->Html->link(__d('default','Nowy Tag'),array('action'=>'add','?'=>array_merge($this->request->query,array('redirect'=>$_SERVER['REQUEST_URI']))),array('class'=>'add','data-role'=>'button','data-inline'=>'true','data-icon'=>'plus')); ?>
</nav>
