-- --------------------------------------------------------
-- Host:                         odyseusz
-- Server version:               5.5.41-MariaDB - Mageia MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table 1polska.filerepo_fileobjects
CREATE TABLE IF NOT EXISTS `filerepo_fileobjects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `creator_id` bigint(20) unsigned NOT NULL,
  `modelname` enum('Temp','Filerepo.Fileobject','Representative') COLLATE utf8_polish_ci NOT NULL,
  `foreign_key` bigint(20) NOT NULL,
  `scope` varchar(32) COLLATE utf8_polish_ci DEFAULT NULL,
  `order` float NOT NULL,
  `metadata` text COLLATE utf8_polish_ci,
  `is_image` tinyint(1) NOT NULL DEFAULT '0',
  `access_key` char(32) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `order` (`order`),
  KEY `modelname_foreign_key` (`modelname`,`foreign_key`),
  CONSTRAINT `filerepo_fileobjects_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `user_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.filerepo_fileparts
CREATE TABLE IF NOT EXISTS `filerepo_fileparts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fileobject_id` bigint(20) unsigned NOT NULL,
  `filedata` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fileobject_id` (`fileobject_id`),
  CONSTRAINT `filerepo_fileparts_ibfk_3` FOREIGN KEY (`fileobject_id`) REFERENCES `filerepo_fileobjects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.filerepo_thumbs
CREATE TABLE IF NOT EXISTS `filerepo_thumbs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image_id` bigint(20) unsigned NOT NULL,
  `thumbnail_id` bigint(20) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `crop` tinyint(1) NOT NULL DEFAULT '0',
  `watermark` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `wymiary` (`image_id`,`width`,`height`,`crop`),
  KEY `thumbnail_id` (`thumbnail_id`),
  CONSTRAINT `filerepo_thumbs_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `filerepo_fileobjects` (`id`),
  CONSTRAINT `filerepo_thumbs_ibfk_2` FOREIGN KEY (`thumbnail_id`) REFERENCES `filerepo_fileobjects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.postulates
CREATE TABLE IF NOT EXISTS `postulates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `description` tinytext COLLATE utf8_polish_ci NOT NULL,
  `content` text COLLATE utf8_polish_ci,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `creator_user_id` bigint(20) unsigned NOT NULL,
  `modifier_user_id` bigint(20) unsigned NOT NULL,
  `status` enum('not_active','active','deleted') COLLATE utf8_polish_ci NOT NULL DEFAULT 'active',
  `vote_count` bigint(20) unsigned NOT NULL DEFAULT '0',
  `upvotes` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_postulates_user_users` (`creator_user_id`),
  KEY `FK_postulates_user_users_2` (`modifier_user_id`),
  KEY `status` (`status`),
  KEY `vote_count` (`vote_count`),
  KEY `upvotes` (`upvotes`),
  KEY `created` (`created`),
  CONSTRAINT `FK_postulates_user_users` FOREIGN KEY (`creator_user_id`) REFERENCES `user_users` (`id`),
  CONSTRAINT `FK_postulates_user_users_2` FOREIGN KEY (`modifier_user_id`) REFERENCES `user_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.postulates_users
CREATE TABLE IF NOT EXISTS `postulates_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `postulate_id` bigint(20) unsigned NOT NULL,
  `value` tinyint(4) NOT NULL,
  `last_change_id` bigint(20) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_postulate_id` (`user_id`,`postulate_id`),
  KEY `user_id_postulate_id_value` (`postulate_id`,`value`,`user_id`),
  KEY `user_id_value` (`user_id`,`value`),
  KEY `value` (`value`),
  CONSTRAINT `FK_postulates_users_postulates` FOREIGN KEY (`postulate_id`) REFERENCES `postulates` (`id`),
  CONSTRAINT `FK_postulates_users_user_users` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.postulates_users_changes
CREATE TABLE IF NOT EXISTS `postulates_users_changes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `postulates_user_id` bigint(20) unsigned NOT NULL,
  `value` tinyint(4) NOT NULL,
  `host` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `emaildomain` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `postulates_user_id_created` (`postulates_user_id`,`created`),
  KEY `created` (`created`),
  KEY `host` (`host`),
  KEY `emaildomain` (`emaildomain`),
  CONSTRAINT `FK_postulates_users_changes_postulates_users` FOREIGN KEY (`postulates_user_id`) REFERENCES `postulates_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.representatives
CREATE TABLE IF NOT EXISTS `representatives` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `lastname` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `description` tinytext COLLATE utf8_polish_ci NOT NULL,
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `creator_user_id` bigint(20) unsigned NOT NULL,
  `modifier_user_id` bigint(20) unsigned NOT NULL,
  `status` enum('not_active','active','deleted') COLLATE utf8_polish_ci NOT NULL DEFAULT 'active',
  `vote_count` bigint(20) unsigned NOT NULL DEFAULT '0',
  `upvotes` bigint(20) unsigned NOT NULL DEFAULT '0',
  `downvotes` bigint(20) unsigned NOT NULL DEFAULT '0',
  `vote_rate` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_representatives_user_users` (`creator_user_id`),
  KEY `FK_representatives_user_users_2` (`modifier_user_id`),
  KEY `status` (`status`),
  KEY `vote_count` (`vote_count`),
  KEY `upvotes` (`upvotes`),
  KEY `downvotes` (`downvotes`),
  KEY `vote_rate` (`vote_rate`),
  KEY `created` (`created`),
  CONSTRAINT `FK_representatives_user_users` FOREIGN KEY (`creator_user_id`) REFERENCES `user_users` (`id`),
  CONSTRAINT `FK_representatives_user_users_2` FOREIGN KEY (`modifier_user_id`) REFERENCES `user_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.representatives_users
CREATE TABLE IF NOT EXISTS `representatives_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `representative_id` bigint(20) unsigned NOT NULL,
  `value` tinyint(4) NOT NULL,
  `last_change_id` bigint(20) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_representative_id` (`user_id`,`representative_id`,`value`),
  KEY `user_id_representative_id_value` (`representative_id`,`value`,`user_id`),
  KEY `user_id_value` (`user_id`,`value`),
  KEY `value` (`value`),
  CONSTRAINT `FK_representatives_users_representatives` FOREIGN KEY (`representative_id`) REFERENCES `representatives` (`id`),
  CONSTRAINT `FK_representatives_users_user_users` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.representatives_users_changes
CREATE TABLE IF NOT EXISTS `representatives_users_changes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `representatives_user_id` bigint(20) unsigned NOT NULL,
  `value` tinyint(4) NOT NULL,
  `host` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `emaildomain` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `representatives_user_id_created` (`representatives_user_id`,`created`),
  KEY `host` (`host`),
  KEY `emaildomain` (`emaildomain`),
  KEY `created` (`created`),
  CONSTRAINT `FK_representatives_users_changes_representatives_users` FOREIGN KEY (`representatives_user_id`) REFERENCES `representatives_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.tags_foreigns
CREATE TABLE IF NOT EXISTS `tags_foreigns` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) unsigned NOT NULL,
  `modelname` enum('Representative') COLLATE utf8_polish_ci NOT NULL,
  `foreign_key` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id_modelname_foreign_key` (`tag_id`,`modelname`,`foreign_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.


-- Dumping structure for table 1polska.user_users
CREATE TABLE IF NOT EXISTS `user_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `emailhash` char(32) COLLATE utf8_polish_ci NOT NULL,
  `emaildomain` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `status` enum('not_active','active','deleted') COLLATE utf8_polish_ci NOT NULL DEFAULT 'active',
  `created` datetime DEFAULT NULL,
  `modified` datetime NOT NULL,
  `metadata` longtext COLLATE utf8_polish_ci,
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `avatar_from` varchar(32) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `emailhash` (`emailhash`),
  KEY `status` (`status`),
  KEY `emaildomain` (`emaildomain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
