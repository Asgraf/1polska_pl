<?php
/**
 * Routes configuration
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 * PHP 5
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AsgrafRouter', 'Asgraf.Lib');
AsgrafRouter::addPrefix('prefix', Router::prefixes());

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::parseExtensions('json','ajax','jpeg');
Router::connect('/', array('plugin'=>false, 'controller'=>'home', 'action'=>'main'));
Router::connect('/auth_callback/*', array('controller'=>'home','action'=>'auth_callback'));
Router::connect('/admin', array('controller'=>'users', 'action'=>'index','admin'=>true,'prefix'=>'admin'));
Router::connect('/kontakt', array('plugin'=>false, 'controller'=>'pages', 'action'=>'display', 'kontakt'));
Router::connect('/o_co_chodzi', array('plugin'=>false, 'controller'=>'pages', 'action'=>'display', 'o_co_chodzi'));
Router::connect('/feedback', array('plugin'=>false, 'controller'=>'pages', 'action'=>'display', 'disqus'));
Router::connect('/polityka_prywatnosci', array('plugin'=>false, 'controller'=>'pages', 'action'=>'display', 'polityka_prywatnosci'));
Router::connect('/postulaty/historia_glosowania', array('plugin'=>false, 'controller'=>'PostulatesUsersChanges','action'=>'index'));
Router::connect('/postulaty/:id/edycja', array('plugin'=>false, 'controller'=>'postulates','action'=>'edit'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/postulaty/:slug_:id/edycja', array('plugin'=>false, 'controller'=>'postulates','action'=>'edit'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/postulaty/:slug_:id/*', array('plugin'=>false, 'controller'=>'postulates','action'=>'view'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/postulaty/:id/*', array('plugin'=>false, 'controller'=>'postulates','action'=>'view'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/postulaty/nowy/*', array('plugin'=>false, 'controller'=>'postulates','action'=>'add'));
Router::connect('/postulaty/glos/poparcie/:id', array('plugin'=>false, 'controller'=>'postulates','action'=>'upvote'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/postulaty/glos/brak_poparcia/:id', array('plugin'=>false, 'controller'=>'postulates','action'=>'downvote'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/postulaty/glos/anuluj_glos/:id', array('plugin'=>false, 'controller'=>'postulates','action'=>'cancelvote'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/postulaty/propozycje_uzytkownikow/*', array('plugin'=>false, 'controller'=>'postulates', 'action'=>'index', 'status'=>'not_active'));
Router::connect('/postulaty/*', array('plugin'=>false, 'controller'=>'postulates', 'action'=>'index', 'status'=>'active'));
Router::connect('/postulaty/*', array('plugin'=>false, 'controller'=>'postulates', 'action'=>'index'));
Router::connect('/reprezentanci/historia_glosowania', array('plugin'=>false, 'controller'=>'RepresentativesUsersChanges','action'=>'index'));
Router::connect('/reprezentanci/:id/edycja', array('plugin'=>false, 'controller'=>'representatives','action'=>'edit'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/reprezentanci/:slug_:id/edycja', array('plugin'=>false, 'controller'=>'representatives','action'=>'edit'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/reprezentanci/:slug_:id/*', array('plugin'=>false, 'controller'=>'representatives','action'=>'view'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/reprezentanci/:id/*', array('plugin'=>false, 'controller'=>'representatives','action'=>'view'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/reprezentanci/nowy/*', array('plugin'=>false, 'controller'=>'representatives','action'=>'add'));
Router::connect('/reprezentanci/glos/poparcie/:id', array('plugin'=>false, 'controller'=>'representatives','action'=>'upvote'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/reprezentanci/glos/brak_poparcia/:id', array('plugin'=>false, 'controller'=>'representatives','action'=>'downvote'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/reprezentanci/glos/anuluj_glos/:id', array('plugin'=>false, 'controller'=>'representatives','action'=>'cancelvote'),array('pass'=>array('id'),'id'=>'[0-9]+'));
Router::connect('/reprezentanci/propozycje_uzytkownikow/*', array('plugin'=>false, 'controller'=>'representatives', 'action'=>'index', 'status'=>'not_active'));
Router::connect('/reprezentanci/*', array('plugin'=>false, 'controller'=>'representatives', 'action'=>'index', 'status'=>'active'));
Router::connect('/reprezentanci/*', array('plugin'=>false, 'controller'=>'representatives', 'action'=>'index'));
Router::connect('/uzytkownicy/*', array('controller'=>'users','action'=>'index'));
Router::connect('/zapomniane_haslo', array('controller'=>'users','action'=>'forgot_password'));
Router::connect('/profil', array('controller'=>'users','action'=>'edit'));
Router::connect('/profil/usuwanie', array('controller'=>'users','action'=>'delete'));
Router::connect('/profil/:slug/:emailhash/*', array('controller'=>'users','action'=>'view'),array('pass'=>array('id'),'emailhash'=>'[0-9a-f]{32}'));
Router::connect('/profil/:emailhash/*', array('controller'=>'users','action'=>'view','slug'=>''),array('pass'=>array('id'),'emailhash'=>'[0-9a-f]{32}'));
Router::connect('/admin/profil/:slug/:emailhash/*', array('admin'=>true,'controller'=>'users','action'=>'view'),array('pass'=>array('id'),'emailhash'=>'[0-9a-f]{32}'));
Router::connect('/admin/profil/:emailhash/*', array('admin'=>true,'controller'=>'users','action'=>'view','slug'=>''),array('pass'=>array('id'),'emailhash'=>'[0-9a-f]{32}'));
Router::connect('/rejestracja', array('controller'=>'users','action'=>'register'));
Router::connect('/zaloguj', array('controller'=>'users','action'=>'login'));
Router::connect('/zaloguj/*', array('controller'=>'home','action'=>'social_login'));
Router::connect('/wyloguj', array('controller'=>'users','action'=>'logout'));
Router::connect('/tagi', array('plugin'=>false, 'controller'=>'tags', 'action'=>'index'));
Router::connect('/tagi/:tag', array('plugin'=>false, 'controller'=>'tags', 'action'=>'view'));

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
AsgrafRouter::defaultRoutes();
require CAKE . 'Config' . DS . 'routes.php';
Router::connect('/**', array('controller'=>'pages', 'action'=>'display'));