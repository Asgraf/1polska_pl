<?php
/**
 * FileobjectFixture
 *
 */
class FileobjectFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'size' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'creator_user_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'modelname' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'foreign_key' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'order' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'access_key' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 32, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'portal_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'metadata' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_polish_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'creator_id' => array('column' => 'creator_user_id', 'unique' => 0),
			'order' => array('column' => 'order', 'unique' => 0),
			'portal_id' => array('column' => 'portal_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_polish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'name' => 'Lorem ipsum dolor sit amet',
			'type' => 'Lorem ipsum dolor sit amet',
			'size' => '',
			'created' => '2014-07-23 19:19:56',
			'creator_user_id' => '',
			'modelname' => 'Lorem ipsum dolor sit amet',
			'foreign_key' => '',
			'order' => 1,
			'access_key' => 'Lorem ipsum dolor sit amet',
			'portal_id' => '',
			'metadata' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'
		),
	);

}
