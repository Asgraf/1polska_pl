<?php
/**
 * RepresentativesUsersChangeFixture
 *
 */
class RepresentativesUsersChangeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'representatives_user_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true),
		'value' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_polish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'representatives_user_id' => '',
			'value' => 1,
			'created' => '2014-11-08 17:20:18'
		),
	);

}
