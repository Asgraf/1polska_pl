<?php
App::uses('Fileobject', 'Model');

/**
 * Fileobject Test Case
 *
 */
class FileobjectTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.fileobject',
		'app.creator_user',
		'app.portal',
		'app.filepart'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Fileobject = ClassRegistry::init('Fileobject');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Fileobject);

		parent::tearDown();
	}

}
