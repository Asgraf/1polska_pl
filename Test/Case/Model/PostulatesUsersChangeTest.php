<?php
App::uses('PostulatesUsersChange', 'Model');

/**
 * PostulatesUsersChange Test Case
 *
 */
class PostulatesUsersChangeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.postulates_users_change',
		'app.postulates_user',
		'app.user',
		'app.fileobject',
		'app.thumb',
		'app.filepart',
		'app.postulate'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PostulatesUsersChange = ClassRegistry::init('PostulatesUsersChange');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PostulatesUsersChange);

		parent::tearDown();
	}

}
