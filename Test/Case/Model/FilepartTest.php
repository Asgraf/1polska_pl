<?php
App::uses('Filepart', 'Model');

/**
 * Filepart Test Case
 *
 */
class FilepartTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.filepart',
		'app.fileobject',
		'app.creator_user',
		'app.portal'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Filepart = ClassRegistry::init('Filepart');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Filepart);

		parent::tearDown();
	}

}
