<?php
App::uses('PostulatesUser', 'Model');

/**
 * PostulatesUser Test Case
 *
 */
class PostulatesUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.postulates_user',
		'app.user',
		'app.postulate',
		'app.creator_user',
		'app.modifier_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PostulatesUser = ClassRegistry::init('PostulatesUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PostulatesUser);

		parent::tearDown();
	}

}
