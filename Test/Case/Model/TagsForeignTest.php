<?php
App::uses('TagsForeign', 'Model');

/**
 * TagsForeign Test Case
 *
 */
class TagsForeignTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tags_foreign',
		'app.tag'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TagsForeign = ClassRegistry::init('TagsForeign');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TagsForeign);

		parent::tearDown();
	}

}
