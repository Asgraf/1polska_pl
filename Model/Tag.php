<?php
App::uses('AppModel', 'Model');
/**
 * Tag Model
 *
 * @property Tag $ParentTag
 * @property Tag $ChildTag
 */
class Tag extends AppModel {
	public $actsAs=array('Asgraf.Modelname');
	public $displayField = 'name';

	public function __construct($id=false,$table=null,$ds=null) {

		/**
		 * Validation rules
		 */
		$this->validate=array(
			'name'=>array(
				'notBlank'=>array(
					'rule'=>array('notBlank'),
					'message'=>__d('validation','notBlank'),
				),
				'minlength'=>array(
					'rule'=>array('minLength',2),
					'message'=>__d('validation','minlength %s','2'),
					'allowEmpty'=>true,
				),
				'maxlength'=>array(
					'rule'=>array('maxLength',50),
					'message'=>__d('validation','maxlength %s','50'),
					'allowEmpty'=>true,
				),
				'unique'=>array(
					'rule'=>array('isUnique'),
					'message'=>__d('validation','unique'),
					'allowEmpty'=>true,
				),
			)
		);
		parent::__construct($id,$table,$ds);
		$this->virtualFields['tag_count']='SELECT count(1) FROM tags_foreigns AS TagsForeignsCount WHERE TagsForeignsCount.tag_id='.$this->alias.'.id';
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany=array(
		'TagsForeign'=>array(
			'className'=>'TagsForeign',
			'foreignKey'=>'tag_id',
			'dependent'=>true,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		)
	);

	public $hasAndBelongsToMany = array(
		'Postulate' => array(
			'className' => 'Postulate',
			'joinTable' => 'tags_foreigns',
			'with'=>'TagsForeign',
			'foreignKey' => 'tag_id',
			'associationForeignKey' => 'foreign_key',
			'unique' => 'keepExisting',
			'conditions' => array('TagsForeign.modelname'=>'Postulate'),
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Representative' => array(
			'className' => 'Representative',
			'joinTable' => 'tags_foreigns',
			'with'=>'TagsForeign',
			'foreignKey' => 'tag_id',
			'associationForeignKey' => 'foreign_key',
			'unique' => 'keepExisting',
			'conditions' => array('TagsForeign.modelname'=>'Representative'),
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
	);

	public function beforeValidate($options=array()) {
		return parent::beforeValidate($options);
	}

	public function beforeSave($options=array()) {
		return parent::beforeSave($options);
	}
}
