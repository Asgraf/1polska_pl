<?php
App::uses('AppModel', 'Model');
/**
 * PostulatesUser Model
 *
 * @property User $User
 * @property Postulate $Postulate
 * @property PostulatesUsersChange $Change
 */
class PostulatesUser extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Postulate' => array(
			'className' => 'Postulate',
			'foreignKey' => 'postulate_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'LastChange' => array(
			'className' => 'PostulatesUsersChange',
			'foreignKey' => 'last_change_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany=array(
		'Change'=>array(
			'className'=>'PostulatesUsersChange',
			'foreignKey'=>'postulates_user_id',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		),
	);

	public function afterSave($created, $options = array()) {
		$current_value = Hash::get($this->data,$this->alias.'.value');
		$this->Postulate->updateVoteCache($this->data['PostulatesUser']['postulate_id']);
		if($current_value!==null) {
			$latest_change_value = Hash::get($this->Change->find('first',array(
				'fields'=>'Change.value',
				'order'=>'Change.created DESC'
			)),'Change.value');

			if($current_value!==$latest_change_value) {
				$this->Change->create();
				if($this->Change->save(array(
					'postulates_user_id'=>$this->id?:$this->getLastInsertID(),
					'value'=>$this->data[$this->alias]['value'],
					'host'=>Router::getRequest()->clientIp(!Configure::read('TrustProxy')),
					'emaildomain'=>AuthComponent::user('emaildomain')
				))) {
					$this->saveField('last_change_id',$this->Change->getLastInsertID(),array('callbacks'=>false));
				} else {
					return false;
				}
			}
		}
	}
}
