<?php
App::uses('CakeEmail', 'Network/Email');
class EmailPasswordBehavior extends ModelBehavior {
	private $password = array();
	
	public function beforeSave (Model $Model,$options=array()) {
		if(isset($Model->data[$Model->alias]['password'])) {
			$this->password[$Model->alias] = $Model->data[$Model->alias]['password'];
		}
		return true;
	}
	public function afterSave(Model $Model, $created, $options = array()) {
		if(!empty($Model->data[$Model->alias]['register'])) {
			$email = new CakeEmail('default');
			$email->template('register');
			$email->viewVars(array('username'=>$Model->data[$Model->alias][$Model->displayField],'email'=>Hash::get($Model->data,$Model->alias.'.email'),'password'=>$this->password[$Model->alias]));
			$email->to($Model->data[$Model->alias]['email']);
			$email->subject(__d('user','Welcome to %s',$_SERVER['HTTP_HOST']));
			$email->send();
		}
		unset($this->password[$Model->alias]);
	}
}