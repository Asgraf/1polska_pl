<?php
App::uses('AppModel', 'Model');
/**
 * Postulate Model
 *
 * @property User $CreatorUser
 * @property User $ModifierUser
 * @property User $User
 * @property PostulatesUser $PostulatesUser
 */
class Postulate extends AppModel {
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxLength' => array(
				'rule' => array('maxLength',50),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique'=>array(
				'rule'=>array('isUnique'),
			),
		),
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxLength' => array(
				'rule' => array('maxLength',160),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'CreatorUser' => array(
			'className' => 'User',
			'foreignKey' => 'creator_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ModifierUser' => array(
			'className' => 'User',
			'foreignKey' => 'modifier_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany=array(
		'PostulatesUser'=>array(
			'className'=>'PostulatesUser',
			'foreignKey'=>'postulate_id',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		),
		'TagsForeign'=>array(
			'className'    => 'TagsForeign',
			'foreignKey'  => 'foreign_key',
			'dependent'    => true,
			'conditions'=> array('TagsForeign.modelname'=>'Postulate'),
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		)
	);
	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var array
	 */
	public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'postulates_users',
			'foreignKey' => 'postulate_id',
			'associationForeignKey' => 'user_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Tag' => array(
			'className' => 'Tag',
			'joinTable' => 'tags_foreigns',
			'with'=>'TagsForeign',
			'foreignKey' => 'foreign_key',
			'associationForeignKey' => 'tag_id',
			'unique' => 'keepExisting',
			'conditions' => array('TagsForeign.modelname'=>'Postulate'),
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

	function updateVoteCache($id) {
		$vote_count = $this->PostulatesUser->find('count', array(
			'conditions'=>array(
				'PostulatesUser.postulate_id'=>$id,
				'PostulatesUser.value'=>1,
				'User.status'=>'active'
			),
			'contain'=>'User.status'
		));
		$upvotes = $this->PostulatesUser->find('count', array(
			'conditions'=>array(
				'PostulatesUser.postulate_id'=>$id,
				'PostulatesUser.value'=>1,
				'User.status'=>'active'
			),
			'contain'=>'User.status'
		));
		$this->id = $id;
		$this->save(
			array(
				'vote_count'=>$vote_count,
				'upvotes'=>$upvotes,
			),
			array('callbacks'=>false)
		);
	}


	function getRelatedRepresentatives($id) {
		$connected_representatives = $this->User->Representative->find('list',[
			'fields'=>['id','connected_user_id'],
			'conditions'=>[
				'Representative.connected_user_id IS NOT NULL'
			]
		]);
		$connected_representatives_votes = $this->PostulatesUser->find('all',[
			'conditions'=>[
				'PostulatesUser.postulate_id'=>$id,
				'PostulatesUser.value'=>1,
				'PostulatesUser.user_id'=>$connected_representatives
			],
			'contain'=>[
				'User'=>[
					'fields'=>['id'],
					'ConnectedRepresentative'=>[
						'fields'=>['id','name','upvotes'],
						'Photo'
					]
				]
			]
		]);
		return Hash::sort(Hash::extract($connected_representatives_votes,'{n}.User.ConnectedRepresentative'),'upvotes','desc');
	}
}
