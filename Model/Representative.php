<?php
App::uses('AppModel', 'Model');
/**
 * Representative Model
 *
 * @property User $CreatorUser
 * @property User $ModifierUser
 * @property RepresentativesUser $RepresentativesUser
 */
class Representative extends AppModel {
	public $displayField = 'name';
	public $actsAs = array(
		'Filerepo.FileField'=>array(
			'photo'=>array(
				'data'=>array(
					'is_image'=>true,
				),
				'required'=>true,
				'validation'=>array(
					'max_width'=>800,
					'max_height'=>600
				)
			)
		)
	);

	function __construct($id=false,$table=null,$ds=null){
		parent::__construct($id,$table,$ds);
		$this->virtualFields['name'] = sprintf('CONCAT(%s.firstname," ",%s.lastname)',$this->alias,$this->alias);
	}
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'firstname' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxLength' => array(
				'rule' => array('maxLength',20),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'lastname' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxLength' => array(
				'rule' => array('maxLength',20),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'maxLength' => array(
				'rule' => array('maxLength',160),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'content' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'image' => array(
			'uploadError' => array(
				'rule'    => 'uploadError',
			)
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'CreatorUser' => array(
			'className' => 'User',
			'foreignKey' => 'creator_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ModifierUser' => array(
			'className' => 'User',
			'foreignKey' => 'modifier_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ConnectedUser' => array(
			'className' => 'User',
			'foreignKey' => 'connected_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany=array(
		'RepresentativesUser'=>array(
			'className'=>'RepresentativesUser',
			'foreignKey'=>'representative_id',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		),
		'TagsForeign'=>array(
			'className'    => 'TagsForeign',
			'foreignKey'  => 'foreign_key',
			'dependent'    => true,
			'conditions'=> array('TagsForeign.modelname'=>'Representative'),
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		)
	);

	public $hasOne = array(
		'Photo'=>array(
			'className'=>'Filerepo.Fileobject',
			'foreignKey'=>'foreign_key',
			'dependent'=>true,
			'conditions'=>array(
				'Photo.modelname'=>'Representative',
				'Photo.scope'=>'photo'
			),
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		),
	);

	public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'representatives_users',
			'foreignKey' => 'representative_id',
			'associationForeignKey' => 'user_id',
			'unique' => 'keepExisting',
			'conditions'=> '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Tag' => array(
			'className' => 'Tag',
			'joinTable' => 'tags_foreigns',
			'with'=>'TagsForeign',
			'foreignKey' => 'foreign_key',
			'associationForeignKey' => 'tag_id',
			'unique' => 'keepExisting',
			'conditions' => array('TagsForeign.modelname'=>'Representative'),
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

	function updateVoteCache($id) {
		$vote_count = $this->RepresentativesUser->find('count', array(
			'conditions'=>array(
				'RepresentativesUser.representative_id'=>$id,
				'RepresentativesUser.value'=>1,
				'User.status'=>'active'
			),
			'contain'=>'User.status'
		));
		$upvotes = $this->RepresentativesUser->find('count', array(
			'conditions'=>array(
				'RepresentativesUser.representative_id'=>$id,
				'RepresentativesUser.value'=>1,
				'User.status'=>'active'
			),
			'contain'=>'User.status'
		));
		$downvotes = $this->RepresentativesUser->find('count', array(
			'conditions'=>array(
				'RepresentativesUser.representative_id'=>$id,
				'RepresentativesUser.value'=>-1,
				'User.status'=>'active'
			),
			'contain'=>'User.status'
		));
		$vote_rate = $upvotes+$downvotes?$upvotes/($upvotes+$downvotes):0;
		$this->id = $id;
		$this->save(
			array(
				'vote_count'=>$vote_count,
				'upvotes'=>$upvotes,
				'downvotes'=>$downvotes,
				'vote_rate'=>$vote_rate
			),
			array('callbacks'=>false)
		);
	}
}
