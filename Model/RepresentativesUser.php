<?php
App::uses('AppModel', 'Model');
/**
 * RepresentativesUser Model
 *
 * @property User $User
 * @property Representative $Representative
 * @property RepresentativesUsersChange $Change
 */
class RepresentativesUser extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Representative' => array(
			'className' => 'Representative',
			'foreignKey' => 'representative_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'LastChange' => array(
			'className' => 'RepresentativesUsersChange',
			'foreignKey' => 'last_change_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany=array(
		'Change'=>array(
			'className'=>'RepresentativesUsersChange',
			'foreignKey'=>'representatives_user_id',
			'dependent'=>false,
			'conditions'=>'',
			'fields'=>'',
			'order'=>'',
			'limit'=>'',
			'offset'=>'',
			'exclusive'=>'',
			'finderQuery'=>'',
			'counterQuery'=>''
		),
	);

	public function afterSave($created, $options = array()) {
		$this->Representative->updateVoteCache($this->data['RepresentativesUser']['representative_id']);
		$current_value = Hash::get($this->data,$this->alias.'.value');
		if($current_value!==null) {
			$latest_change_value = Hash::get($this->Change->find('first',array(
				'fields'=>'Change.value',
				'order'=>'Change.created DESC'
			)),'Change.value');

			if($current_value!==$latest_change_value) {
				$this->Change->create();
				if($this->Change->save(array(
					'representatives_user_id'=>$this->id?:$this->getLastInsertID(),
					'value'=>$this->data[$this->alias]['value'],
					'host'=>Router::getRequest()->clientIp(!Configure::read('TrustProxy')),
					'emaildomain'=>AuthComponent::user('emaildomain')
				))) {
					$this->saveField('last_change_id',$this->Change->getLastInsertID(),array('callbacks'=>false));
				} else {
					return false;
				}
			}
		}
	}
}
