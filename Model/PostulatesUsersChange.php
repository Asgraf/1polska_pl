<?php
App::uses('AppModel', 'Model');
/**
 * PostulatesUsersChange Model
 *
 * @property PostulatesUser $PostulatesUser
 */
class PostulatesUsersChange extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'value' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'PostulatesUser' => array(
			'className' => 'PostulatesUser',
			'foreignKey' => 'postulates_user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function afterFind($results, $primary = false) {
		$request = Router::getRequest();
		if($request['ext']) {
			foreach($results as &$result) {
				if(isset($result['$this->alias']['host'])) {
					$ip=$result[$this->alias]['host'];
					if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
						$ip_m=explode('.', $ip);
						array_pop($ip_m);
						array_push($ip_m, '••');
						$result[$this->alias]['ip']=implode('.', $ip_m);
						$host=gethostbyaddr_cached($ip);
						if($host!=$ip) {
							$host_m=explode('.', $host);
							if(count($host_m)>2) {
								$host_m[0]='••••';
								$result[$this->alias]['hostname']=implode('.', $host_m);
							}
						}
					} elseif(filter_var($result[$this->alias]['host'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
						$ip_m=explode(':', $ip);
						array_pop($ip_m);
						array_push($ip_m, '••');
						$result[$this->alias]['ip']=implode(':', $ip_m);
					}
					unset($result[$this->alias]['host']);
				}
				if(isset($result['User']['emailhash'])) {
					$result['User']['profile_url']=Router::url(['controller'=>'users','action'=>'view','emailhash'=>$result['User']['emailhash'],'slug'=>'']);
					unset($result['User']['emailhash']);
				}
				unset($result['PostulatesUser']['id']);
			}
		}
		return $results;
	}
}
