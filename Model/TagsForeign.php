<?php
App::uses('AppModel', 'Model');
/**
 * TagsForeign Model
 *
 * @property Tag $Tag
 */
class TagsForeign extends AppModel {
	public $actsAs=array('Asgraf.Modelname','Asgraf.Enum');

	public function __construct($id=false,$table=null,$ds=null) {
		/**
		 * Validation rules
		 */
		$this->validate=array(
			'tag_id'=>array(
				'notBlank'=>array(
					'rule'=>array('notBlank'),
					'message'=>__d('validation','notBlank'),
					'allowEmpty'=>true,
				),
				'numeric'=>array(
					'rule'=>array('numeric'),
					'message'=>__d('validation','numeric'),
					'allowEmpty'=>true,
				),
				'minval'=>array(
					'rule'=>array('comparison','>=',1),
					'message'=>__d('validation','minval %s %s','>=','1'),
					'allowEmpty'=>true,
				),
				'maxval'=>array(
					'rule'=>array('comparison','<=',9223372036854775807),
					'message'=>__d('validation','maxval %s %s','<=','9223372036854775807'),
					'allowEmpty'=>true,
				),
			),
			'modelname'=>array(
				'notBlank'=>array(
					'rule'=>array('notBlank'),
					'message'=>__d('validation','notBlank'),
					'allowEmpty'=>true,
				),
			),
			'foreign_key'=>array(
				'notBlank'=>array(
					'rule'=>array('notBlank'),
					'message'=>__d('validation','notBlank'),
					'allowEmpty'=>true,
				),
				'numeric'=>array(
					'rule'=>array('numeric'),
					'message'=>__d('validation','numeric'),
					'allowEmpty'=>true,
				),
				'minval'=>array(
					'rule'=>array('comparison','>=',0),
					'message'=>__d('validation','minval %s %s','>=','0'),
					'allowEmpty'=>true,
				),
				'maxval'=>array(
					'rule'=>array('comparison','<=',9223372036854775807),
					'message'=>__d('validation','maxval %s %s','<=','9223372036854775807'),
					'allowEmpty'=>true,
				),
			),
		);
		parent::__construct($id,$table,$ds);
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo=array(
		'Tag'=>array(
			'className'=>'Tag',
			'foreignKey'=>'tag_id',
			'conditions'=>'',
			'fields'=>'',
			'order'=>''
		)
	);
	public function beforeValidate($options=array()) {
		return parent::beforeValidate($options);
	}

	public function beforeSave($options=array()) {
		return parent::beforeSave($options);
	}
}
