<?php
App::uses('Model', 'Model');
/**
 * Class AppModel
 *
 * @method contain
 * @method begin
 * @method rollback
 * @method commit
 * @method getEnumValues
 * @method getModelnameValues
 */
class AppModel extends Model {
	public $actsAs = array(
		'Containable',
		'Asgraf.UserLogger',
		'Asgraf.Enum',
	);
	public $recursive = -1;

	public function __construct($id = false, $table = null, $ds = null) {
		foreach(array('hasOne','hasMany','belongsTo','hasAndBelongsToMany') as $assoc) {
			foreach($this->$assoc as $alias=>$assoc_data) {
				if(!empty($assoc_data['with'])) {
					list($plugin) = pluginSplit($assoc_data['with']);
					if($plugin && !CakePlugin::loaded($plugin)) {
						unset($this->{$assoc}[$alias]);
					}
				}
				list($plugin) = pluginSplit($assoc_data['className']);
				if($plugin && !CakePlugin::loaded($plugin)) {
					unset($this->{$assoc}[$alias]);
				}
			}
		}
		parent::__construct($id,$table,$ds);
	}

	public function plugin() {
		$parentClass = get_parent_class($this);
		if(strpos($parentClass,'AppModel')===false) return null;
		return str_replace('AppModel','',$parentClass);
	}

	public function pluginPrefix() {
		$plugin = $this->plugin();
		return $plugin?$plugin.'.':'';
	}

	public function delete($id = null, $cascade = true) {
		$schema = $this->schema();
		if(empty($schema['status'])) {
			return parent::delete($id,$cascade);
		} else {
			$this->id = $id;
			return $this->save([$this->alias=>['status'=>'deleted']],false);
		}
	}

	public function trash($id = null, $cascade = true) {
		return parent::delete($id,$cascade);
	}

	public function deleteAll($conditions, $cascade = true, $callbacks = false) {
		$schema = $this->schema();
		if(empty($schema['status'])) {
			return parent::deleteAll($conditions, $cascade, $callbacks);
		} else {
			return $this->updateAll(array('status'=>'deleted'),$conditions);
		}
	}
}
