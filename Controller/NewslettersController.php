<?php
App::uses('AppController','Controller');
/**
 * Newsletters Controller
 * @property Newsletter $Newsletter
 * @property array paginate
 */
class NewslettersController extends AppController {
	public $guestActions = array();

	/**
	* index method
	* @param array $conditions
	* @return CakeResponse
	*/
	private function _index($conditions=array()) {
		$conditions = array_merge($this->Search->getSearchConditions(),$conditions);
		$options = array(
			'conditions'=>$conditions,
		);
		if(!empty($this->request['named']['all']) && ($this->request->is('ajax') || $this->request['requested'])) {
			$newsletters = $this->Newsletter->find('all',$options);
			$this->set('all',1);
		} else {
			$this->paginate = array_merge($this->paginate,$options);
			$newsletters = $this->Paginator->paginate();
			$this->set('all',0);
		}
		unset($this->request->params['named']['all']);
		$this->set('newsletters',$newsletters);
		return null;
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return CakeResponse
	 */
	private function _view($id = null) {
		if(!$this->Newsletter->exists($id)) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		$options = array(
			'conditions'=>array('Newsletter.'.$this->Newsletter->primaryKey=>$id),
		);
		$newsletter = $this->Newsletter->find('first',$options);
		$this->set('newsletter',$newsletter);
		$this->set('title_for_layout',$newsletter['Newsletter']['subject']);

		if(!empty($this->request->params['requested'])) {
			return $newsletter;
		}
		$status = $this->Newsletter->getEnumValues('status');
		$this->set(compact('status'));
		if(empty($this->request->params['ext']) && $newsletter['Newsletter']['status']=='queted') {
			$this->response->header("refresh:1");
		}
		return null;
	}

	/**
	* search method
	* @return void
	*/
	private function _search() {
		$users = $this->Newsletter->User->find('list');
		$status = $this->Newsletter->getEnumValues('status');
		$this->set(compact('users','status'));
	}

	function admin_send_test_email($id) {
		$this->request->allowMethod('post');
		if(!$this->Newsletter->exists($id)) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		$newsletter = $this->Newsletter->read(null,$id);
		$cakemail=new CakeEmail('default');
		$cakemail->to(AuthComponent::user('email'));
		$cakemail->subject($newsletter['Newsletter']['subject']);
		$cakemail->emailFormat('html');
		if($cakemail->send($newsletter['Newsletter']['txt'])) {
			$this->Message->flash(__('Wysłano mail testowy'),['action'=>'index'],'good');
		} else {
			$this->Message->flash(__('Problem z wysyłaniem maila'),['action'=>'index'],'bad');
		}
	}

	function admin_mass_assign_all_users($id) {
		$this->request->allowMethod('post');
		if(!$this->Newsletter->exists($id)) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		$result = $this->Newsletter->query("INSERT INTO newsletters_users (newsletter_id,user_id) SELECT ".intval($id).",id FROM user_users WHERE email IS NOT NULL AND status='active'");
		$this->redirect(['action'=>'index']);
	}

	private function _emailsToUserIds($emails) {
		$emails = Hash::filter(explode("\r\n",trim($emails)));
		$user_ids = $this->Newsletter->User->find('list',array(
			'fields'=>'User.id',
			'conditions'=>array(
				'User.email'=>array_unique($emails)
			)
		));
		return array_keys($user_ids);
	}

	/**
	 * add method
	 * @param null $id
	 * @return void
	 */
	private function _add($id=null) {
		if($this->request->is('post') || $this->request->is('put')) {
			/*$user_ids = $this->_emailsToUserIds($this->request->data('Newsletter.emails'));
			if(!empty($user_ids)) {
				$this->request->data['User']['User']=$user_ids;
			}*/
			$this->Newsletter->create();
			if($this->Newsletter->save($this->request->data)) {
				$metadata = array('Newsletter'=>array($this->Newsletter->primaryKey=>$this->Newsletter->getLastInsertId()));
				$this->Message->flash(__('The newsletter has been saved'),array('action'=>'index'),'good',$metadata);
				return;
			} else {
				$this->Message->flash(__('The newsletter could not be saved. Please, try again.'),null,'bad');
			}
		} elseif(!empty($id)) {
			$options = array(
				'conditions'=> array('Newsletter.'.$this->Newsletter->primaryKey=>$id)
			);
			$newsletter = $this->Newsletter->find('first',$options);
			$this->request->data = $newsletter;
		}
		//$users = $this->Newsletter->User->find('list');
		$status = $this->Newsletter->getEnumValues('status');
		$this->set(compact('users','status'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	private function _edit($id=null) {
		if(!$this->Newsletter->exists($id)) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		if($this->request->is('post') || $this->request->is('put')) {
			/*$user_ids = $this->_emailsToUserIds($this->request->data('Newsletter.emails'));
			if(!empty($user_ids)) {
				$this->request->data['User']['User']=$user_ids;
			}*/
			$this->Newsletter->id = $id;
			if($metadata = $this->Newsletter->save($this->request->data)) {
				$this->Message->flash(__('The newsletter has been saved'),$this->referer(array('action'=>'index')),'good');
				return;
			} else {
				$this->Message->flash(__('The newsletter could not be saved. Please, try again.'),null,'bad');
			}
		} else {
			$options = array(
				'conditions'=> array('Newsletter.'.$this->Newsletter->primaryKey=>$id),
				'contain'=>array(
					'User.email'
				),
			);
			$newsletter = $this->Newsletter->find('first',$options);
			$newsletter['Newsletter']['emails']=implode("\r\n",Hash::extract($newsletter,'User.{n}.email'));
			$this->set('title_for_layout',$newsletter['Newsletter']['subject']);
			$this->request->data = $newsletter;
		}
		$users = $this->Newsletter->User->find('list');
		$status = $this->Newsletter->getEnumValues('status');
		$this->set(compact('users','status'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @param string $id
	 * @return void
	 */
	private function _delete($id=null) {
		$this->request->allowMethod('post','delete');
		if(empty($id) && !empty($this->request->data)) {
			if(empty($this->request->data['Newsletter'][0])) {
				$this->Message->flash(__('No newsletters selected'),$this->referer(array('action'=>'index')),'neutral');
			} elseif($this->Newsletter->deleteAll(array('Newsletter.'.$this->Newsletter->primaryKey=>$this->request->data['Newsletter']))) {
				$this->Message->flash(__('Newsletters deleted'),array('action'=>'index'),'good');
			} else {
				$this->Message->flash(__('Newsletters not deleted'),$this->referer(array('action'=>'index')),'bad');
			}
			return;
		}
		$this->Newsletter->id = $id;
		if(!$this->Newsletter->exists()) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		if($this->Newsletter->delete($id)) {
			$this->Message->flash(__('Newsletter deleted'),$this->referer(array('action'=>'index')),'good');
		} else {
			$this->Message->flash(__('Newsletter was not deleted'),$this->referer(array('action'=>'index')),'bad');
		}
	}

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index() {
		$this->_index();
	}

	/**
	 * admin_search method
	 *
	 * @return void
	 */
	public function admin_search() {
		$this->_search();
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @internal param string $id
	 * @return void
	 */
	public function admin_view() {
		$this->_view($this->request['id']);
	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add() {
		$this->_add($this->request['id']);
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @internal param string $id
	 * @return void
	 */
	public function admin_edit() {
		$this->_edit($this->request['id']);
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @internal param string $id
	 * @return void
	 */
	public function admin_delete() {
		$this->_delete($this->request['id']);
	}
}