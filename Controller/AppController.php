<?php
App::uses('Controller', 'Controller');
App::uses('CrudControllerTrait', 'Crud.Lib');
/**
 * Class AppController
 * @property MessageComponent   $Message
 * @property SearchComponent    $Search
 * @property PaginatorComponent $Paginator
 * @property RedirectComponent  $Redirect
 * @property CrudComponent  $Crud
 * @property RecaptchaComponent  $Recaptcha
 * @property CakeRequest  $request
 * @property User  $User
 */
class AppController extends Controller {
	use CrudControllerTrait;
	public $persistModel = true;

	public $components=[
		'RequestHandler',
		'Session',
		'Recaptcha',
		'Security',
		'Auth'=>[
			'authenticate'=>[
				'Form'=>[
					'userModel'=>'User',
					'fields'=>[
						'username'=>'email'
					],
					'contain'=>[
						'ConnectedRepresentative'=>[
							'fields'=>['id','name','status','modified']
						],
						'PostulatesUser',
						'RepresentativesUser'
					],
					'scope'=>[
						'User.status'=>'active'
					],
					'passwordHasher'=>'Blowfish'
				]
			],
			'authorize'=>'Controller',
			'loginAction'=>['admin'=>false,'controller'=>'users','action'=>'login'],
			'logoutRedirect'=>['admin'=>false,'controller'=>'home','action'=>'main'],
		],
		'AsgrafMessage.Message',
		'AsgrafSearch.Search',
		'Paginator'=>['paramType'=>'querystring'],
		'Asgraf.Redirect',
	];

	public $helpers=[
		'Form'=>[
			'className'=>'BoostCake.BoostCakeForm',
			'defaultOptions'=>[
				'inputDefaults' => [
					'div' => 'form-group',
					'label' => [
						'class' => 'col col-md-3 control-label'
					],
					'wrapInput' => 'col col-md-9',
					'class' => 'form-control'
				],
				'class' => 'well form-horizontal'
			]
		],
		'Html'=>['className'=>'BoostCake.BoostCakeHtml'],
		'Paginator'=>['className'=>'BoostCake.BoostCakePaginator'],
		'Session',
		'Asgraf.Format',
		'Asgraf.Gravatar'=>['default'=>'identicon'],
	];

	public $guestActions=[];

	public function __construct($request = null, $response = null) {
		if(Configure::read('debug') && CakePlugin::loaded('DebugKit')) $this->components['DebugKit.Toolbar']=['panels'=>['history'=>false]];
		return parent::__construct($request,$response);
	}

	private function _setup_page_id() {
		$url_elements=array($this->request['plugin'] ? $this->request['plugin'].'_plugin' : 'app', $this->request['controller'].'_controller', $this->request['action'].'_action');
		if($this->request['id'])
			$url_elements[]='id'.$this->request['id'];
		$this->set('page_id', implode('-', $url_elements));
		$this->set('page_class', implode(' ', $url_elements));
		$this->set('random_id', rand());
	}

	private function slugRedirect() {
		$id = $this->request['id'];
		if($id && !$this->request['ext'] && $this->request['controller']!='fileobjects' && !in_array($this->request['action'],['upvote','downvote','cancelvote'])) {
			/**
			 * @var $Model AppModel
			 */
			$Model = $this->{$this->modelClass};
			$slug = slug(Hash::get($Model->read($Model->displayField,$id),$Model->alias.'.'.$Model->displayField));
			if($slug && $slug!=$this->request['slug']) {
				$this->redirect(array('slug'=>$slug)+paramsToUrl($this->request->params),301);
			}
		}
	}

	private function alternativeRouteRedirect() {
		if(!$this->request->is('ajax')) {
			$this->slugRedirect();
			$current_url = env('REQUEST_URI');
			$correct_url = Router::url(parseUrl($current_url));
			if(urldecode($current_url)!=urldecode($correct_url)) {
				$this->redirect($correct_url,301);
			}
		}
	}

	private function checkCaptcha() {
		if(in_array($this->request['action'],['upvote','downvote','cancelvote'])) return;
		if(!$this->Recaptcha->checkCaptcha()) {
			$this->Message->flash(__('Nieprawidłowa Catpcha'),$this->referer('/',true),'bad');
		}
	}

	private  function upgradeSessionNeeded() {
		if(AuthComponent::user('id')) {
			$session = AuthComponent::user();
			if(!array_key_exists('postulate_vote_count',$session)) return true;
		}
		return false;
	}

	function beforeFilter() {
		if($this->upgradeSessionNeeded()) {
			$this->renewUserSession();
		}
		if($this->toString()=='CakeErrorController') return;
		$this->alternativeRouteRedirect();
		$this->checkCaptcha();
		if(in_array($this->request['action'], $this->guestActions)) {
			$this->Auth->allow();
		}

		if($this->Components->enabled('Security')) {
			$this->Security->blackHoleCallback = 'blackhole';
			$this->Security->csrfExpires = '+1 hour';
		}
		$this->_setup_page_id();
	}

	function beforeRender() {
		if(!Hash::get($this->viewVars,'title_for_layout')) {
			if(substr($this->request['action'],-5)=='index') {
				$this->viewVars['title_for_layout'] = __($this->name);
			} else {
				$this->viewVars['title_for_layout'] = __(Inflector::singularize($this->name));
			}
		};
	}

	function blackhole($type) {
		if($type=='secure' && $this->request->method()=='GET') {
			return $this->redirect('https://'.env('SERVER_NAME').$_SERVER['REQUEST_URI']);
		}
		$exception = new ForbiddenException(__('%s Security Error',Inflector::humanize($type)));
		if(in_array($type,array('auth','csrf'))) {
			return $this->Message->flash(__('This request has been blackholed due to a %s Security Error.',Inflector::humanize($type)),$this->request->referer(true),'bad');
		} else {
			throw $exception;
		}
	}

	protected function vote_id_key() {
		if(AuthComponent::user('PostulatesUser.0')) {
			$this->Session->write('Auth.User.PostulatesUser',Hash::combine(AuthComponent::user('PostulatesUser'),'{n}.postulate_id','{n}'));
		}
		if(AuthComponent::user('RepresentativesUser.0')) {
			$this->Session->write('Auth.User.RepresentativesUser',Hash::combine(AuthComponent::user('RepresentativesUser'),'{n}.representative_id','{n}'));
		}
	}

	protected function renewUserSession($id=null) {
		$userModel = Hash::get($this->Auth->settings,'authenticate.Form.userModel');
		$this->loadModel($userModel);
		$this->{$userModel}->recursive=0;
		$primaryKey = $this->{$userModel}->primaryKey;
		$id = $id?:AuthComponent::user($primaryKey);
		if(!$id) return false;
		$options = [
			'conditions'=>[
				$userModel.'.'.$primaryKey=>$id
			]+Hash::get($this->Auth->settings,'authenticate.Form.scope')?:[]
		]+Hash::get($this->Auth->settings,'authenticate.Form');
		unset($options['fields']);
		unset($options['scope']);
		unset($options['passwordHasher']);
		$user = $this->{$userModel}->find('first',$options);
		if(!empty($user)) {
			$user = array_merge($user[$userModel],$user);
			unset($user[$userModel]);
			$user['session_cache_time']=time();
			unset($user['password']);
			$this->Session->write('Auth.'.$userModel,array_merge(AuthComponent::user()?:array(),$user));
			$this->afterLogin();
			return true;
		} else {
			$this->Session->delete('Auth.'.$userModel);
			return false;
		}
	}

	protected function afterLogin() {
		$this->vote_id_key();
		$this->updatePid();
		$this->loadModel('User');
		$vote_count = $this->User->PostulatesUser->find('count',[
			'conditions'=>[
				'PostulatesUser.user_id'=>AuthComponent::user('id'),
				'PostulatesUser.value'=>1,
			]
		]);
		$this->Session->write('Auth.User.postulate_vote_count',$vote_count);
	}

	public function isAuthorized($user) {
		if(empty($this->request['prefix']))
			return true;
		if($this->request['prefix'] && $user['admin'])
			return true;
		return false;
	}

	public function referer($default = null, $local = false) {
		$referer = parent::referer();
		if(Router::url($this->Auth->loginAction,true)==$referer) return $default;
		return $referer;
	}

	protected function storeLoginReferer() {
		if(!$this->Session->read('Auth.redirect')) {
			$referer = parseUrl($this->request->referer(),true);
			if(is_array($referer) && !in_array($referer['action'],['login','social_login','logout','auth_callback']) && empty($referer['ext'])) {
				$this->Session->write('Auth.redirect',$referer);
			}
		}
	}

	protected function updatePid() {
		$pid = CakeSession::read('pid');
		$upid = AuthComponent::user('pid')?:AuthComponent::user('id');
		if(!$upid) return;
		if($pid) {
			if($upid!=$pid) {
				$this->loadModel('User');
				if(!$this->User->updateAll(['User.pid'=>$upid],['OR'=>['User.pid'=>$pid,'User.id'=>$pid]])) {
					throw new InternalErrorException('pid save error');
				}
			}
		}
		CakeSession::write('pid',$upid);
	}
}