<?php
App::uses('Component', 'Controller');
App::uses('HttpSocket', 'Network/Http');

class RecaptchaComponent extends Component {
	/** @var AppController */
	private $controller;

	public function initialize(Controller $controller) {
		$this->controller = $controller;
		if($controller->Components->loaded('Security')) {
			$controller->Security->unlockedFields[]='g-recaptcha-response';
		}
	}

	public function checkCaptcha() {
		$request = Router::getRequest();
		if($request['admin']) return true;
		if($request->is('post') || $request->is('put')) {
			$recapcha = $request->data('g-recaptcha-response');
			if(empty($recapcha)) {
				return false;
			}
			try {
				$HttpSocket = new HttpSocket();
				$result = $HttpSocket->get('https://www.google.com/recaptcha/api/siteverify', array(
					'secret'=>Configure::read('Recaptcha.secret_key'),
					'response'=>$recapcha,
					'remoteip'=>$request->clientIp(!Configure::read('TrustProxy'))
				));
				$result = json_decode($result->body,true);
				if(!is_array($result) || !array_key_exists('success',$result)) {
					return false;
				}
				return $result['success'];
			} catch(SocketException $e) {
				//problem z usługą recaptcha
				$this->controller->Message->flash('Nie udało się zweryfikować Captchy',null,'flash');
				return false;
			}
		}
		return true;
	}
} 