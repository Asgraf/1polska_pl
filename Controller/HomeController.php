<?php
App::uses('AppController', 'Controller');

/**
 * Class HomeController
 * @property ExtAuthComponent $ExtAuth
 */
class HomeController extends AppController {
	public $guestActions=array('main','social_login','auth_callback');
	public $components = array('ExtAuth.ExtAuth');

	public function main() {
		$postulates = ClassRegistry::init('Postulate')->find('all', [
			'conditions'=>[
				'Postulate.status'=>'active'
			],
			'order'=>'upvotes DESC',
			'limit'=>6,
		]);
		$representatives = ClassRegistry::init('Representative')->find('all',array(
			'conditions'=>[
				'Representative.status'=>'active'
			],
			'order'=>'upvotes DESC',
			'limit'=>6,
			'contain'=>[
				'Photo',
			],
		));
		$users_count = ClassRegistry::init('User')->find('count');
		$this->set(compact('postulates','representatives','users_count'));
	}

	public function social_login($provider) {
		$this->storeLoginReferer();
		$result = $this->ExtAuth->login($provider);
		if ($result['success']) {
			$this->redirect($result['redirectURL']);
		} else {
			$this->Session->setFlash($result['message']);
			$this->redirect($this->Auth->loginAction);
		}
	}

	public function auth_callback($provider) {
		$result = $this->ExtAuth->loginCallback($provider);
		if ($result['success']) {
			$this->__successfulExtAuth($result['profile'], $result['accessToken']);
		} else {
			$this->Session->setFlash($result['message']);
			$this->redirect($this->Auth->loginAction);
		}
	}

	private function __successfulExtAuth($incomingProfile, $accessToken) {
		if(empty($incomingProfile['email']) || !filter_var($incomingProfile['email'],FILTER_VALIDATE_EMAIL)) {
			return $this->Message->flash(__('%s API odmówiło dostępu do twojego adresu email. Logowanie nie powiodło się',$incomingProfile['provider']),$this->Auth->redirectUrl(),'bad');
		}
		$incomingProfile['raw']=json_decode($incomingProfile['raw'],true)?:$incomingProfile['raw'];
		$this->loadModel('User');
		$data_to_save = $incomingProfile+array('metadata'=>array($incomingProfile['provider']=>$incomingProfile));
		if(empty($data_to_save['username'])) {
			$data_to_save['username'] =explode('@',$incomingProfile['email'])[0];
		}
		$data_to_save_register = $data_to_save+[
				'register'=>true,
				'created'=>date('Y-m-d H:i:s'),
				'status'=>'active',
				'avatar_from'=>$incomingProfile['provider'],
				'password'=>generatePassword()
			];
		$this->User->Behaviors->disable('Status');
		$user = $this->User->findByEmailhashAndEmaildomain(md5(strtolower($incomingProfile['email'])),getEmailDomain($incomingProfile['email']));
		if(!$user) {
			$this->User->create($data_to_save_register);
		} else {
			if($user['User']['status']=='not_active') {
				return $this->Message->flash(__('To konto jest nieaktywne. Nie można się na nie zalogować'),$this->Auth->redirectUrl(),'bad');
			}
			$this->User->create(false);
			$this->User->id = $user['User']['id'];
			if($user['User']['status']=='deleted'){
				$this->User->set($data_to_save_register);
			} else {
				$this->User->set($data_to_save);
			}
		}
		if($this->User->save()) {
			if($this->renewUserSession($this->User->id)) {
				return $this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Message->flash(__d('user','Logowanie za pomocą %s\'a nie powiodło się',$incomingProfile['provider']),$this->Auth->loginAction,'bad');
			}
		} else {
			CakeLog::critical(__('Failed to save profile %s with due to following error %s',json_encode($this->User->data),json_encode($this->User->validationErrors)));
			throw new InternalErrorException('Failed to save profile');
		}
	}
} 