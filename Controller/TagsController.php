<?php
App::uses('AppController','Controller');
/**
 * Tags Controller
 * @property Tag $Tag
 */
class TagsController extends AppController {
	public $guestActions = array('index','view');
	public $components = [
		'Crud.Crud' => [
			'actions' => [
				'admin_index'=>'Crud.Index',
				'admin_add'=>'Crud.Add',
				'admin_edit'=>'Crud.Edit',
				'admin_delete'=>'Crud.Delete',
			]
		]
	];

	public function isAuthorized($user) {
		$parent = parent::isAuthorized($user);
		if(!$parent) return false;

		return true;
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('displayField','name');
	}

	public function index() {
		$tags = $this->Tag->find('list',['order'=>'Tag.name','fields'=>['Tag.name','Tag.tag_count']]);
		$this->set('tags',$tags);
		$this->set('_serialize','tags');
		return $tags;
	}

	public function view() {
		$options = array(
			'fields'=>array('Tag.id','Tag.name'),
			'conditions'=>array('Tag.'.$this->Tag->displayField=>$this->request['tag']),
			'contain'=>array(
				'Postulate'=>[
					'fields'=>['id','name'],
					'conditions'=>[
							'status'=>['active','not_active']
						]+$this->Tag->hasAndBelongsToMany['Postulate']['conditions'],
					'order'=>'upvotes DESC',
				],
				'Representative'=>[
					'fields'=>['id','name'],
					'conditions'=>[
							'status'=>['active','not_active']
						]+$this->Tag->hasAndBelongsToMany['Representative']['conditions'],
					'order'=>'upvotes DESC',
					'Photo'
				],
			),
		);
		$tag = $this->Tag->find('first',$options);
		if(empty($tag)) {
			throw new NotFoundException(__d('default','Invalid tag'));
		}
		$this->set('tag',$tag);
		$this->set('title_for_layout',$tag['Tag']['name']);

		$this->set('_serialize','tag');
		return $tag;
	}
}
