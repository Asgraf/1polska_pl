<?php
App::uses('AppController','Controller');
/**
 * Users Controller
 * @property User $User
 * @property array paginate
 */
class UsersController extends AppController {
	public $guestActions = ['add','register','login','forgot_password','getAvatar'];

    public function isAuthorized($user) {
		$parent = parent::isAuthorized($user);
		if(!$parent) return false;
		if(!empty($this->request['id'] && $this->request['action']=='edit')) {
			if($this->request['id']!=AuthComponent::user('id')) return false;
		}

        return true;
    }

	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('displayField','username');
	}


	/**
	* index method
	* @param array $conditions
	* @return array|bool|null|CakeResponse
	*/
	private function _index($conditions=array()) {
		$conditions = array_merge($this->Search->getSearchConditions(),$conditions);
		$options = array(
			'conditions'=>$conditions,
			'contain'=>array(),
		);
		if(!empty($this->request['named']['all']) && ($this->request->is('ajax') || $this->request['requested'])) {
			$users = $this->User->find('all',$options);
			$this->set('all',1);
		} else {
			$this->paginate = array_merge($this->paginate,$options);
			$users = $this->Paginator->paginate();
			$this->set('all',0);
		}
		unset($this->request->params['named']['all']);
		$this->set('users',$users);
		return $users;
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @return array|bool|null|CakeResponse
	 */
	private function _view() {
		if(empty($this->request['emailhash'])) {
			throw new NotFoundException(null,401);
		}
		$options = [
			'conditions'=>[
				'User.emailhash'=>$this->request['emailhash'],
				'User.status'=>['active','not_active']
			],
			'contain'=>[
				'Postulate'=>[
					'fields'=>['id','name'],
					'conditions'=>[
						'status'=>['active','not_active']
					],
					'order'=>'upvotes DESC',
				],
				'Representative'=>[
					'fields'=>['id','name'],
					'conditions'=>[
						'status'=>['active','not_active']
					],
					'order'=>'upvotes DESC',
					'Photo'
				],
				'ConnectedRepresentative'=>[
					'fields'=>['id','firstname','lastname','status'],
					'conditions'=>[
						'ConnectedRepresentative.status'=>['active','not_active']
					],
				]
			],
		];
		if($this->request['ext']) {
			unset($options['contain']['Postulate']);
			unset($options['contain']['Representative']);
			$options['fields']=['username','emailhash','emaildomain','status','created','modified','metadata','avatar_from'];
		}
		$user = $this->User->find('first',$options);
		if($this->request['ext']) {
			$user['User']['picture_url']=getAvatarUrl($user['User'],'ROZMIAR_OBRAZKA');
			unset($user['User']['emailhash']);
			unset($user['User']['metadata']);
			unset($user['User']['avatar_from']);
			$this->response->expires('+1 minute');
			$this->response->sharable(true,MINUTE);
		}

		if(empty($user)) {
			return $this->Message->flash('Konto którego szukasz zostało usunięte lub nigdy nie istniało',$this->request->referer(true));
		}
		if($this->request['slug']!=slug($user['User']['username']) && !$this->request['ext']) {
			return $this->redirect(['action'=>'view','emailhash'=>$this->request['emailhash'],'slug'=>slug($user['User']['username'])]);
		}
		if(!empty($user['ConnectedRepresentative']['id']) && !$this->request['ext']) {
			return $this->redirect(['controller'=>'representatives','action'=>'view','id'=>$user['ConnectedRepresentative']['id'],'slug'=>slug($user['ConnectedRepresentative']['firstname'].' '.$user['ConnectedRepresentative']['lastname'])]);
		}
		$this->set('user',$user);
		$this->set('_serialize','user');
		$this->set('title_for_layout',$user['User']['username']);

		$status = $this->User->getEnumValues('status');
		$this->set(compact('status'));
		return $user;
	}

	/**
	* search method
	* @return array|bool|null
	*/
	private function _search() {
		$status = $this->User->getEnumValues('status');
		$this->set(compact('status'));
		return null;
	}

	/**
	 * add method
	 * @param null $id
	 * @return array|bool|null
	 */
	private function _add($id=null) {
		if($this->request->is('post')) {
			$this->User->create();
			if($this->User->save($this->request->data)) {
				$metadata = array('User'=>array($this->User->primaryKey=>$this->User->getLastInsertId()));
				return $this->Message->flash(__d('user','The user has been saved'),array('action'=>'index'),'good',$metadata);
			} else {
				$this->Message->flash(__d('user','The user could not be saved. Please, try again.'),null,'bad');
			}
		} elseif(!empty($id)) {
			$options = array(
				'fields'=>array('User.id','User.username','User.email','User.status','User.created','User.modified'),
				'conditions'=> array('User.'.$this->User->primaryKey=>$id)
			);
			$user = $this->User->find('first',$options);
			$this->request->data = $user;
		}
		$status = $this->User->getEnumValues('status');
		$this->set(compact('status'));
		return null;
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return array|bool|null
	 */
	private function _edit($id=null) {
		if(!$this->User->exists($id)) {
			throw new NotFoundException(__d('user','Invalid user'));
		}
		$options = array(
			'fields'=>array('User.id','User.username','User.email','User.status','User.created','User.modified','User.metadata','User.avatar_from'),
			'conditions'=> array('User.'.$this->User->primaryKey=>$id),
			'contain'=>array(
				'ConnectedRepresentative.id'
			),
		);
		$user = $this->User->find('first',$options);

		if(($this->request->is('post') || $this->request->is('put'))) {
			$this->User->id = $id;
			if($metadata = $this->User->save($this->request->data)) {
				if($id==AuthComponent::user('id')) {
					$this->renewUserSession($id);
				}
				return $this->Message->flash(__d('user','The user has been saved'),$this->referer(array('action'=>'edit',$id)),'good');
			} else {
				$this->Message->flash(__d('user','The user could not be saved. Please, try again.'),null,'bad');
			}
		} else {
			$this->request->data = $user;
		}
		$status = $this->User->getEnumValues('status');
		$this->set(compact('status','user'));
		$this->set('title_for_layout',$this->request->data('User.username'));
		return null;
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @param string $id
	 * @return array|bool|null
	 */
	private function _delete($id=null) {
		$this->request->onlyAllow('post','delete');
		if(!$this->User->exists($id)) {
			throw new NotFoundException(__d('user','Invalid user'));
		}
		try {
			if($this->User->trash($id)) {
				return $this->Message->flash(__d('user','User deleted'),'/','good');
			} else {
				return $this->Message->flash(__d('user','User was not deleted'),null,'bad');
			}
		} catch(Exception $e) {
			if($this->User->delete($id)) {
				return $this->Message->flash(__d('user','User deleted'),'/','good');
			} else {
				return $this->Message->flash(__d('user','User was not deleted'),null,'bad');
			}

		}
	}

	/**
	 * deleteMany method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @return array|bool|null
	 */
	private function _deleteMany() {
		$this->request->onlyAllow('post','delete');
		if(empty($this->request->data['User'][0])) {
			return $this->Message->flash(__d('user','No users selected'),array('action'=>'index'),'neutral');
		} elseif($this->User->deleteAll(array('User.'.$this->User->primaryKey=>$this->request->data['User']))) {
			return $this->Message->flash(__d('user','Users deleted'),array('action'=>'index'),'good');
		} else {
			return $this->Message->flash(__d('user','Users not deleted'),array('action'=>'index'),'bad');
		}
	}

	/**
	 * register method
	 *
	 * @return void|null
	 */
	private function _register() {
		if($this->request->is('post')) {
			if(!empty($this->request->data['User'])) {
				$data_to_save = $this->request->data['User']+[
						'register'=>true,
						'created'=>date('Y-m-d H:i:s'),
						'status'=>'active',
						'avatar_from'=>null,
						'password'=>generatePassword()
					];
				$this->User->set($data_to_save);
				if($this->User->validates()) {
					$email = strtolower($this->request->data('User.email'));
					$this->User->Behaviors->disable('Status');
					$user = $this->User->findByEmailhashAndEmaildomain(md5($email),getEmailDomain($email));
					if(!$user) {
						$this->User->create($data_to_save);
					} else {
						if($user['User']['status']=='deleted'){
							$this->User->create(false);
							$this->User->id = $user['User']['id'];
							$this->User->set($data_to_save);
						} else {
							return $this->Message->flash('Już istnieje konto przypisane do tego adresu email',null,'bad');
						}
					}
					if($this->User->save()) {
						$this->Message->flash(__d('user','Your account has been successfully created. Check your email to complete registration progress.'),'/','good');
					}  else {
						$this->Message->flash(__d('user','Registration failed. Please, try again.'),null,'bad');
					}
				}
			}
		}
	}


	/**
	 * login method
	 *
	 * @return null
	 */
	private function _login() {
		if(AuthComponent::user('id')) {
			return $this->redirect($this->Auth->redirectUrl());
		}
		if($this->request->is('post')) {
			if($this->Auth->login()) {
				$this->afterLogin();
				return $this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Message->flash(__d('user','Username or password is incorrect'),null,'bad');
			}
		} else {
			$this->storeLoginReferer();
		}
		return null;
	}

	/**
	 * logout method
	 *
	 * @return null
	 */
	private function _logout() {
		return $this->redirect($this->Auth->logout());
	}

/**
	 * index method
	 *
	 * @return array|bool|null|CakeResponse
	 */
	public function index() {
		return $this->_index();
	}

	/**
	 * search method
	 *
	 * @return array|bool|null|CakeResponse
	 */
	public function search() {
		return $this->_search();
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @internal param string $id
	 * @return array|bool|null|CakeResponse
	 */
	public function view() {
		return $this->_view();
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @internal param string $id
	 * @return array|bool|null|CakeResponse
	 */
	public function edit() {
		$this->request->params['id']=AuthComponent::user('id');
		return $this->_edit(AuthComponent::user('id'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @return array|bool|null|CakeResponse
	 */
	public function delete() {
		if(!AuthComponent::user('id')) {
			return $this->redirect('/');
		}
		$user = AuthComponent::user();
		if(in_array($this->request->method(),['DELETE','POST'])) {
			$this->Auth->logout();
			$this->_delete($user['id']);
			$this->Message->flash(__('Żegnaj %s!',h($user['username'])));

		}
		$this->set('title_for_layout','Usuwanie konta');
	}

	/**
	 * login method
	 *
	 * @return void|null
	 */
	public function login() {
		if(AuthComponent::user('id')) {
			return $this->redirect('/');
		}
		$this->_login();
	}

	/**
	 * logout method
	 *
	 * @return void
	 */
	public function logout() {
		$this->_logout();
	}

	/**
	 * register method
	 *
	 * @return void|null
	 */
	public function register() {
		if(AuthComponent::user('id')) {
			return $this->redirect('/');
		}
		$this->_register();
	}

	/**
	 * admin_index method
	 *
	 * @return array|bool|null|CakeResponse
	 */
	public function admin_index() {
		return $this->_index();
	}

	/**
	 * admin_search method
	 *
	 * @return array|bool|null|CakeResponse
	 */
	public function admin_search() {
		return $this->_search();
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @internal param string $id
	 * @return array|bool|null|CakeResponse
	 */
	public function admin_view() {
		return $this->_view();
	}

	/**
	 * admin_add method
	 *
	 * @return array|bool|null|CakeResponse
	 */
	public function admin_add() {
		return $this->_add($this->request['id']);
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @internal param string $id
	 * @return array|bool|null|CakeResponse
	 */
	public function admin_edit() {
		$representatives = $this->User->Representative->find('list',[
			'conditions'=>[
				'OR'=>[
					'Representative.connected_user_id'=>$this->request['id'],
					'Representative.connected_user_id IS NULL'
				]
			]
		]);
		$this->set('representatives',$representatives);
		return $this->_edit($this->request['id']);
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @throws MethodNotAllowedException
	 * @internal param string $id
	 * @return array|bool|null|CakeResponse
	 */
	public function admin_delete() {
		return $this->_delete($this->request['id']);
	}

	function forgot_password() {
		if(AuthComponent::user('id')) {
			$this->redirect(array('action'=>'edit'));
		}
		if(!empty($this->request->query)) {
			$user = $this->User->findByEmail($this->request->query('email'));
			$token = Hash::get($user,'User.metadata.forgot_password.token');
			$expires = Hash::get($user,'User.metadata.forgot_password.expires');
			if($user && $token && $this->request->query('token')==$token && $expires>time()) {
				$this->User->id = $user['User']['id'];
				if(!$this->User->saveField('metadata',['forgot_password'=>null])) {
					throw new InternalErrorException('metadata not saved');
				}
				$this->renewUserSession($user['User']['id']);
				$this->Message->flash('Zalogowano za pomocą jednorazowego linka',['controller'=>'users','action'=>'edit']);
			} else {
				return $this->Message->flash(__('Nieważny token.'),array('action'=>'forgot_password'),'bad');
			}
		}
		if($this->request->is('post')) {
			$email = $this->request->data('User.email');
			$user = $user = $this->User->findByEmail($email);
			if(empty($user['User']['id'])) {
				$this->User->invalidate('email',__("Nie znaleźliśmy konta powiązanego z podanym przez ciebie adresem email"));
			} else {
				$expires = time()+HOUR;
				$token = generatePassword(40);
				$this->User->id = $user['User']['id'];
				if($this->User->saveField('metadata',['forgot_password'=>['token'=>$token,'expires'=>$expires]])) {
					App::uses('CakeEmail', 'Network/Email');
					$cakemail = new CakeEmail('default');
					$cakemail->template('forgot_password');
					$cakemail->viewVars(['token'=>$token,'email'=>$email]);
					$cakemail->to($email);
					$cakemail->subject(__('Zapomniane hasło'));
					$cakemail->emailFormat('both');
					$cakemail->send();
					return $this->Message->flash(__('Link do logowania został wysłany na twój adres email'),array('controller'=>'home','action'=>'main'),'neutral');
				} else {
					throw new InternalErrorException('metadata not saved');
				}
			}
		}
		$this->set('title_for_layout','Zapomniane hasło');
	}

	public function admin_pid() {
		$pids = Hash::extract($this->User->find('all',[
			'fields'=>['DISTINCT User.pid'],
			'conditions'=>[
				'User.status !='=>'not_active',
				'User.pid IS NOT NULL'
			],
		]),'{n}.User.pid');
		$options = array(
			'conditions'=>['User.id'=>$pids],
			'contain'=>[
				'ChildUser'=>[
					'order'=>'ChildUser.id DESC'
				]
			],
			'limit'=>10,
			'order'=>'User.id DESC'
		);
		if(!empty($this->request['named']['all']) && ($this->request->is('ajax') || $this->request['requested'])) {
			$users = $this->User->find('all',$options);
			$this->set('all',1);
		} else {
			$this->paginate = array_merge($this->paginate,$options);
			$users = $this->Paginator->paginate();
			$this->set('all',0);
		}
		unset($this->request->params['named']['all']);
		$this->set('users',$users);
		return $users;
	}
}