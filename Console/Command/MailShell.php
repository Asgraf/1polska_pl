<?php
App::uses('AppShell', 'Console/Command');
App::uses('CakeEmail', 'Network/Email');
/**
 * MailShell
 * 
 * @author Michal Turek <asgraf@gmail.com>
 */
/**
 * @property Newsletter $Newsletter
 */
class MailShell extends AppShell {
	/**
	 * @var Newsletter $Newsletter
	 */
	public $Newsletter;

	function main() {
		$this->doNewsletterMailJobs();
		$this->out('Ponowne sprawdzenie listy zadań mailowych za 60 sekund');
		sleep(60);
		$this->main();
	}

	function doNewsletterMailJobs() {
		if(empty($this->Newsletter)) $this->Newsletter = ClassRegistry::init('Newsletter');
		$this->Newsletter->contain(array(
			'NewslettersUser'=>array(
				'conditions'=>array('NewslettersUser.sent'=>false),
				'User.email'
			)
		));
		$newsletters = $this->Newsletter->findAllByStatus('queted');
		if(empty($newsletters)) {
			$this->out('Brak zadań mailowych do wykonania');
			return;
		}
		foreach($newsletters as $newsletter) {
			$this->out('Rozpoczynam zadanie mailowe - '.$newsletter['Newsletter']['subject']);
			foreach($newsletter['NewslettersUser'] as $NewslettersUser) {
				$cakemail=new CakeEmail('default');
				$cakemail->to($NewslettersUser['User']['email']);
				$cakemail->subject($newsletter['Newsletter']['subject']);
				$cakemail->emailFormat('html');
				if($cakemail->send($newsletter['Newsletter']['txt'])) {
					$this->Newsletter->NewslettersUser->id = $NewslettersUser['id'];
					if(!$this->Newsletter->NewslettersUser->saveField('sent',true)) {
						trigger_error('Problem z oznakowaniem maila jako wysłanego',E_USER_ERROR);
					}
				} else {
					trigger_error('Problem z wysyłaniem maila',E_USER_ERROR);
					break;
				}
			}
			$this->Newsletter->id = $newsletter['Newsletter']['id'];
			if(!$this->Newsletter->saveField('status','sent')) {
				trigger_error('Problem z oznakowaniem zadania mailowego jako wysłanego',E_USER_ERROR);
			}
		}
	}
}